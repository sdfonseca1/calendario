<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 5 AGO 2019
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN:
*  ANOTACIONES:
*/
	require_once("conexion.php");

	class Modelo extends Conexion{

		public function ingresoUsuarioModelo($tabla, $datosModelo){
			/*UTILIDAD: recibe los datos del controlador para traer los resultados de la búsqueda en la BD.
			  PRECONDICION: recibe el parámetro de clave y password del usuario.
			  POSTCONDICIÓN: regresa falso o verdadero si encontró los datos del controlador en la BD.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT clave, contrasena FROM $tabla WHERE clave = :clave AND contrasena = :contrasena");
			$sentencia->bindParam(":clave", $datosModelo["clave"], PDO::PARAM_STR);
			$sentencia->bindParam(":contrasena", $datosModelo["contrasena"], PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetch();
			$sentencia->close();
		}

		// INTEGRANTES
		public function obtenerIntegrantesModelo($tabla, $pagina){
			/*UTILIDAD: obtener los datos de todos los integrantes registrados.
			  PRECONDICION: deben existir registros en la base de datos.
			  POSTCONDICIÓN: devuelve los datos de todos los integrantes registrados.
			*/
			$numero_registros = 10;
			if ($pagina == 0 || $pagina == 1) {
				$inicio = 0;
			}else{
				$inicio = ($pagina-1)*10;
			}
			$sentencia = Conexion::conectar()->prepare("SELECT * FROM $tabla LIMIT $inicio, $numero_registros");
			$sentencia->execute();
			return $sentencia->fetchAll();
		}

		public function obtenerIntegrantesRegistradosModelo($tabla){
			/*UTILIDAD: obtener los datos de todos los integrantes registrados.
			  PRECONDICION: deben existir registros en la base de datos.
			  POSTCONDICIÓN: devuelve los datos de todos los integrantes registrados.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT  clavePub, nombre, apellidoPaterno, apellidoMaterno  FROM $tabla");
			$sentencia->execute();
			return $sentencia->fetchAll();
		}


		public function pruebaModelo($tabla){
			/*UTILIDAD: obtener los datos de un integrante registrados.
			  PRECONDICION: deben existir registros en la base de datos.
			  POSTCONDICIÓN: devuelve los datos de un integrante registrados.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT clavePub, nombre, apellidoPaterno, apellidoMaterno FROM $tabla");
			$sentencia->execute();
			return $sentencia->fetchAll();
		}


		public function obtenerIntegranteModelo($tabla, $clave){
			/*UTILIDAD: obtener los datos de un integrante registrados.
			  PRECONDICION: deben existir registros en la base de datos.
			  POSTCONDICIÓN: devuelve los datos de un integrante registrados.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE clavePub=:clave");
			$sentencia->bindParam(":clave", $clave, PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetch();
		}

		public function obtenerSeleccionIntegrantesModelo($tabla){
			/*UTILIDAD: obtener los datos de un integrante registrados.
			  PRECONDICION: deben existir registros en la base de datos.
			  POSTCONDICIÓN: devuelve los datos de un integrante registrados.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT * FROM $tabla");
			$sentencia->execute();
			var_dump($sentencia->fetchAll());
			return $sentencia->fetchAll();
		}

		public function existeAsignacionModelo($tabla, $datos){
			/*UTILIDAD: obtener los datos de un integrante registrados.
			  PRECONDICION: deben existir registros en la base de datos.
			  POSTCONDICIÓN: devuelve los datos de un integrante registrados.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE claveIntegrante=:integrante AND clavePrograma=:programa AND claveActividad=:actividad");
			$sentencia->bindParam(":integrante", $datos['integrante'], PDO::PARAM_STR);
			$sentencia->bindParam(":programa", $datos['programa'], PDO::PARAM_STR);
			$sentencia->bindParam(":actividad", $datos['actividad'], PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetch();
		}

		public function obtenerSeleccionIntegrantesModelo2($tabla){
			/*UTILIDAD: obtener los datos de un integrante registrados.
			  PRECONDICION: deben existir registros en la base de datos.
			  POSTCONDICIÓN: devuelve los datos de un integrante registrados.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT * FROM $tabla");
			$sentencia->execute();
			return $sentencia->fetchAll();
		}

		public function registrarIntegranteModelo($tabla, $datosModelo){
			/*UTILIDAD: registra los datos de un integrante.
			  PRECONDICION: recibe los datos del controlador,
			  POSTCONDICIÓN: devuelve falso o verdadero si los datos del integrantes fueron registrados o nombre.
			*/
			$sentencia = Conexion::conectar()->prepare("INSERT INTO $tabla VALUES(:clave, :nombre, :paterno, :materno, :sexo)");
			$sentencia->bindParam(":clave", $datosModelo["clave"], PDO::PARAM_STR);
			$sentencia->bindParam(":nombre", htmlentities($datosModelo["nombre"]), PDO::PARAM_STR);
			$sentencia->bindParam(":paterno", htmlentities($datosModelo["paterno"]), PDO::PARAM_STR);
			$sentencia->bindParam(":materno", htmlentities($datosModelo["materno"]), PDO::PARAM_STR);
			$sentencia->bindParam(":sexo", $datosModelo["sexo"], PDO::PARAM_STR);
			var_dump($sentencia);
			return $sentencia->execute();
		}

		public function actualizarIntegranteModelo($tabla, $datosModelo){
			/*UTILIDAD: actualiza los datos de un integrante registrado.
			  PRECONDICION: recibe los datos del controlador.
			  POSTCONDICIÓN: devuelve falso o verdadero si los datos fueron actualizados.
			*/
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET nombre=:nombre, apellidoPaterno=:paterno, apellidoMaterno=:materno, sexo=:sexo WHERE clavePub=:clave");
			var_dump($sentencia);
			$sentencia->bindParam(":clave", $datosModelo["clave"], PDO::PARAM_STR);
			$sentencia->bindParam(":nombre", htmlentities($datosModelo["nombre"]), PDO::PARAM_STR);
			$sentencia->bindParam(":paterno", htmlentities($datosModelo["paterno"]), PDO::PARAM_STR);
			$sentencia->bindParam(":materno", htmlentities($datosModelo["materno"]), PDO::PARAM_STR);
			$sentencia->bindParam(":sexo", $datosModelo["sexo"], PDO::PARAM_STR);
			return $sentencia->execute();
		}

		public function eliminarIntegranteModelo($tabla, $clave){
			/*UTILIDAD: elimina los datos de un integrante registrado.
			  PRECONDICION: la clave del integrante debe existir en la base de datos.
			  POSTCONDICIÓN: devuelve falso o verdadero si el integrante fue eliminado.
			*/
			$sentencia = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE clavePub=:clave");
			$sentencia->bindParam(":clave", $clave, PDO::PARAM_STR);
			return $sentencia->execute();
		}

		public function buscarClaveModelo($tabla, $clave){
			/*UTILIDAD: obtiene los datos de un ntegrantee registrado dada su clave.
			  PRECONDICION: se recibe la clave del controlador.
			  POSTCONDICIÓN: devuelve la clave del integrante.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT clavePub FROM $tabla WHERE clavePub=:clave");
			$sentencia->bindParam(":clave", $clave, PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetch();
		}

		public function contarIntegrantesModelo(){
			/*UTILIDAD: cuenta en número de integrantes registrados.
			  PRECONDICION:
			  POSTCONDICIÓN: regresa el número de registros.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT COUNT(clavePub) FROM integrante");
			$sentencia->execute();
			return $sentencia->fetch();
		}

		// PROGRAMAS
		public function obtenerProgramasModelo($tabla, $pagina){
			/*UTILIDAD: obtener los programas registrados,
			  PRECONDICION: deben existir registros en la base de datos.
			  POSTCONDICIÓN: devuelve los datos de todos los programas registrados.
			*/
			$numero_registros = 10;
			if ($pagina == 0 || $pagina == 1) {
				$inicio = 0;
			}else{
				$inicio = ($pagina-1)*10;
			}
			$sentencia = Conexion::conectar()->prepare("SELECT * FROM $tabla LIMIT $inicio, $numero_registros");
			$sentencia->execute();
			return $sentencia->fetchAll();
		}

		public function buscarClaveProgramaModelo($tabla, $clave){
			/*UTILIDAD: obtiene los datos de un programa registrado dada su clave.
			  PRECONDICION: se recibe la clave del controlador.
			  POSTCONDICIÓN: devuelve la clave del programa.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT clavePrograma FROM $tabla WHERE clavePrograma=:clave");
			$sentencia->bindParam(":clave", $clave, PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetch();
		}

		public function registrarProgramaModelo($tabla, $datosModelo){
			/*UTILIDAD: registra los datos de un programa.
			  PRECONDICION: recibe los datos del controlador,
			  POSTCONDICIÓN: devuelve falso o verdadero si los datos del programa fueron registrados o no.
			*/
			$sentencia = Conexion::conectar()->prepare("INSERT INTO $tabla VALUES(:clave, :programa)");
			$sentencia->bindParam(":clave", $datosModelo["clave"], PDO::PARAM_STR);
			$sentencia->bindParam(":programa", htmlentities($datosModelo["nombre"]), PDO::PARAM_STR);
			return $sentencia->execute();
		}

		public function obtenerProgramaModelo($tabla, $clave){
			/*UTILIDAD: obtener los datos de un programa registrados.
			  PRECONDICION: deben existir registros en la base de datos.
			  POSTCONDICIÓN: devuelve los datos de un programa registrados.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE clavePrograma=:clave");
			$sentencia->bindParam(":clave", $clave, PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetch();
		}

		public function actualizarProgramaModelo($tabla, $datosModelo){
			/*UTILIDAD: actualiza los datos de un programa registrado.
			  PRECONDICION: recibe los datos del controlador.
			  POSTCONDICIÓN: devuelve falso o verdadero si los datos fueron actualizados.
			*/
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET nombre=:nombre WHERE clavePrograma=:clave");
			$sentencia->bindParam(":clave", $datosModelo["clave"], PDO::PARAM_STR);
			$sentencia->bindParam(":nombre", htmlentities($datosModelo["nombre"]), PDO::PARAM_STR);
			var_dump($sentencia->errorInfo());
			return $sentencia->execute();
		}

		public function eliminarProgramaModelo($tabla, $clave){
			/*UTILIDAD: elimina los datos de un programa registrado.
			  PRECONDICION: la clave del programa debe existir en la base de datos.
			  POSTCONDICIÓN: devuelve falso o verdadero si el programa fue eliminado.
			*/
			$sentencia = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE clavePrograma=:clave");
			$sentencia->bindParam(":clave", $clave, PDO::PARAM_STR);
			return $sentencia->execute();
		}

		public function obtenerDespliegueProgramasModelo($tabla){
			/*UTILIDAD: obtener los datos de un programa registrados.
			  PRECONDICION: deben existir registros en la base de datos.
			  POSTCONDICIÓN: devuelve los datos de un programa registrados.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT clavePrograma, nombre FROM $tabla");
			// $sentencia->bindParam(":clave", $clave, PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetchAll();
		}

		public function obtenerDespliegueActividadesModelo($tabla, $programa){
			/*UTILIDAD: obtener los datos de un programa registrados.
			  PRECONDICION: deben existir registros en la base de datos.
			  POSTCONDICIÓN: devuelve los datos de un programa registrados.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT claveActividad, asignacion FROM $tabla WHERE clavePrograma=:programa");
			$sentencia->bindParam(":programa", $programa, PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetchAll();
		}

		public function obtenerDespliegueActividadesModelo2($tabla){
			/*UTILIDAD: obtener los datos de un programa registrados.
			  PRECONDICION: deben existir registros en la base de datos.
			  POSTCONDICIÓN: devuelve los datos de un programa registrados.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT clavePrograma, claveActividad, asignacion FROM $tabla");
			$sentencia->bindParam(":programa", $programa, PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetchAll();
		}

		public function mostrarIntegrantesDisponiblesModelo($tabla, $datos){
			/*UTILIDAD:
			  PRECONDICION:
			  POSTCONDICIÓN:
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT integrante.clavePub, integrante.nombre, integrante.apellidoPaterno, integrante.apellidoMaterno FROM integrante JOIN $tabla ON integrante_actividad.claveIntegrante=integrante.clavePub WHERE NOT EXISTS(SELECT clavePub FROM intinerario WHERE clavePub=integrante.clavePub AND intinerario.fecha=:fecha) ORDER BY ultimaAsignacion ASC");
			$sentencia->bindParam(":fecha", $datos['fecha'], PDO::PARAM_STR);
			// $sentencia->bindParam(":actividad", $datos['actividad'], PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetchAll();
		}

		public function reporteFechaModelo($tabla, $fecha){
			/*UTILIDAD:
			  PRECONDICION:
			  POSTCONDICIÓN:
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT $tabla.fecha, $tabla.clavePub, integrante.nombre, integrante.apellidoPaterno, integrante.apellidoMaterno, actividad.asignacion FROM $tabla JOIN integrante ON $tabla.clavePub=integrante.clavePub JOIN actividad ON $tabla.clavePrograma=actividad.clavePrograma AND $tabla.claveActividad=actividad.claveActividad WHERE fecha=:fecha");
			$sentencia->bindParam(":fecha", $fecha, PDO::PARAM_STR);
			$sentencia->execute();
			// var_dump($sentencia->execute());
			return $sentencia->fetchAll();
		}

		public function buscarClaveActividadModelo($tabla, $clave_prog, $clave_act){
			/*UTILIDAD: obtiene los datos de un programa registrado dada su clave.
			  PRECONDICION: se recibe la clave del controlador.
			  POSTCONDICIÓN: devuelve la clave del programa.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT clavePrograma, claveActividad FROM $tabla WHERE clavePrograma=:clave_prog AND claveActividad=:clave_act");
			$sentencia->bindParam(":clave_prog", $clave_prog, PDO::PARAM_STR);
			$sentencia->bindParam(":clave_act", $clave_act, PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetch();
		}

		// ACTIVIDADES
		public function obtenerActividadesModelo($tabla, $pagina){
			$numero_registros = 10;
			if ($pagina == 0 || $pagina == 1) {
				$inicio = 0;
			}else{
				$inicio = ($pagina-1)*10;
			}
			$sentencia = Conexion::conectar()->prepare("SELECT * FROM $tabla LIMIT $inicio, $numero_registros");
			$sentencia->execute();
			return $sentencia->fetchAll();
		}

		public function contarActividadModelo($tabla){
			/*UTILIDAD: cuenta el número de actividades.
			  PRECONDICION:
			  POSTCONDICIÓN:devuelve el número de actividades registradas.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT COUNT(claveActividad) FROM $tabla");
			$sentencia->execute();
			return $sentencia->fetch();
		}

		public function registrarActividadModelo($tabla, $datosModelo){
			/*UTILIDAD: registra los datos de un actividad.
			  PRECONDICION: recibe los datos del controlador,
			  POSTCONDICIÓN: devuelve falso o verdadero si los datos del actividad fueron registrados o no.
			*/
			$sentencia = Conexion::conectar()->prepare("INSERT INTO $tabla VALUES(:clave_prog, :clave_act, :asignacion, :prioridad)");
			$sentencia->bindParam(":clave_prog", $datosModelo["cve_prog"], PDO::PARAM_STR);
			$sentencia->bindParam(":clave_act", $datosModelo["cve_act"], PDO::PARAM_STR);
			$sentencia->bindParam(":asignacion", $datosModelo["nombre"], PDO::PARAM_STR);
			$sentencia->bindParam(":prioridad", $datosModelo["prioridad"], PDO::PARAM_STR);
			return $sentencia->execute();
		}

		public function obtenerActividadModelo($tabla, $programa, $actividad){
			/*UTILIDAD: obtener los datos de un actividad registrados.
			  PRECONDICION: deben existir registros en la base de datos.
			  POSTCONDICIÓN: devuelve los datos de un actividad registrados.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT * FROM $tabla WHERE clavePrograma=:programa AND claveActividad=:actividad");
			$sentencia->bindParam(":programa", $programa, PDO::PARAM_STR);
			$sentencia->bindParam(":actividad", $actividad, PDO::PARAM_STR);
			$sentencia->execute();
			return $sentencia->fetch();
		}

		public function actualizarActividadModelo($tabla, $datosModelo){
			/*UTILIDAD: actualiza los datos de un actividad registrado.
			  PRECONDICION: recibe los datos del controlador.
			  POSTCONDICIÓN: devuelve falso o verdadero si los datos fueron actualizados.
			*/
			$sentencia = Conexion::conectar()->prepare("UPDATE $tabla SET asignacion=:asignacion, prioridad=:prioridad WHERE clavePrograma=:programa AND claveActividad=:actividad");
			$sentencia->bindParam(":asignacion", $datosModelo["descripcion"], PDO::PARAM_STR);
			$sentencia->bindParam(":prioridad", $datosModelo["prioridad"], PDO::PARAM_STR);
			$sentencia->bindParam(":programa", $datosModelo["programa"], PDO::PARAM_STR);
			$sentencia->bindParam(":actividad", $datosModelo["actividad"], PDO::PARAM_STR);
			// var_dump($sentencia->errorInfo());
			return $sentencia->execute();
		}

		public function eliminarActividadModelo($tabla, $programa, $actividad){
			/*UTILIDAD: elimina los datos de un programa registrado.
			  PRECONDICION: la clave del programa debe existir en la base de datos.
			  POSTCONDICIÓN: devuelve falso o verdadero si el programa fue eliminado.
			*/
			$sentencia = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE clavePrograma=:programa AND claveActividad=:actividad");
			$sentencia->bindParam(":programa", $programa, PDO::PARAM_STR);
			$sentencia->bindParam(":actividad", $actividad, PDO::PARAM_STR);
			return $sentencia->execute();
		}

		public function asignarActividadIntegranteModelo($tabla, $datos){
			/*UTILIDAD: elimina los datos de un programa registrado.
			  PRECONDICION: la clave del programa debe existir en la base de datos.
			  POSTCONDICIÓN: devuelve falso o verdadero si el programa fue eliminado.
			*/
			$nulo = null;
			$sentencia = Conexion::conectar()->prepare("INSERT INTO $tabla VALUES(:claveIntegrante, :clavePrograma, :claveActividad, :ultima, 1)");
			$sentencia->bindParam(":claveIntegrante", $datos['integrante'], PDO::PARAM_STR);
			$sentencia->bindParam(":clavePrograma", $datos['programa'], PDO::PARAM_STR);
			$sentencia->bindParam(":claveActividad", $datos['actividad'], PDO::PARAM_STR);
			$sentencia->bindParam(":ultima", $nulo, PDO::PARAM_STR);
			return $sentencia->execute();
		}

		public function crearFechaAsignacionModelo($tabla, $datos){
			/*UTILIDAD: elimina los datos de un programa registrado.
			  PRECONDICION: la clave del programa debe existir en la base de datos.
			  POSTCONDICIÓN: devuelve falso o verdadero si el programa fue eliminado.
			*/
			$nulo = null;
			$sentencia = Conexion::conectar()->prepare("INSERT INTO $tabla VALUES(:calendario, :fecha, :claveIntegrante, :clavePrograma, :claveActividad)");
			$sentencia->bindParam(":fecha", $datos['fecha'], PDO::PARAM_STR);
			$sentencia->bindParam(":claveIntegrante", $datos['integrante'], PDO::PARAM_STR);
			$sentencia->bindParam(":clavePrograma", $datos['programa'], PDO::PARAM_STR);
			$sentencia->bindParam(":claveActividad", $datos['actividad'], PDO::PARAM_STR);
			$sentencia->bindParam(":calendario", $nulo, PDO::PARAM_STR);
			return $sentencia->execute();
		}

		public function existenRegistrosActIntModelo($tabla){
			/*UTILIDAD: elimina los datos de un programa registrado.
			  PRECONDICION: la clave del programa debe existir en la base de datos.
			  POSTCONDICIÓN: devuelve falso o verdadero si el programa fue eliminado.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT COUNT(claveIntegrante) FROM $tabla");
			$sentencia->execute();
			// var_dump($sentencia->fetch());
			return $sentencia->fetch();
		}

		public function contarProgramasModelo($tabla){
			/*UTILIDAD: cuenta el número de programas.
			  PRECONDICION:
			  POSTCONDICIÓN:devuelve el número de programas registradas.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT COUNT(clavePrograma) FROM $tabla");
			$sentencia->execute();
			return $sentencia->fetch();
		}

		public function reporteAsignacionIntegranteModelo($tabla){
			/*UTILIDAD: cuenta el número de programas.
			  PRECONDICION:
			  POSTCONDICIÓN:devuelve el número de programas registradas.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT integrante.clavePub, integrante.nombre, integrante.apellidoPaterno, integrante.apellidoMaterno, actividad.asignacion, integrante_actividad.clavePrograma, integrante_actividad.claveActividad FROM $tabla JOIN integrante ON integrante_actividad.claveIntegrante=integrante.clavePub JOIN actividad ON integrante_actividad.clavePrograma=actividad.clavePrograma AND integrante_actividad.claveActividad=actividad.claveActividad ORDER BY integrante_actividad.claveIntegrante");
			$sentencia->execute();
			return $sentencia->fetchAll();
		}

		public function eliminarAsignacionIntegranteModelo($tabla, $datos){
			/*UTILIDAD: cuenta el número de programas.
			  PRECONDICION:
			  POSTCONDICIÓN:devuelve el número de programas registradas.
			*/
			$sentencia = Conexion::conectar()->prepare("DELETE FROM $tabla WHERE claveIntegrante=:integrante AND clavePrograma=:programa AND claveActividad=:actividad");
			$sentencia->bindParam(":integrante", $datos['integrante'], PDO::PARAM_STR);
			$sentencia->bindParam(":programa", $datos['programa'], PDO::PARAM_STR);
			$sentencia->bindParam(":actividad", $datos['actividad'], PDO::PARAM_STR);
			return $sentencia->execute();
		}

		public function mostrarIntegrantesPorActividadModelo($tabla){
			/*UTILIDAD: cuenta el número de programas.
			  PRECONDICION:
			  POSTCONDICIÓN:devuelve el número de programas registradas.
			*/
			$sentencia = Conexion::conectar()->prepare("SELECT clavePub, nombre, apellidoPaterno, apellidoMaterno FROM integrante_actividad JOIN integrante ON integrante_actividad.claveIntegrante=integrante.clavePub ORDER BY ultimaAsignacion DESC");
			// $sentencia->bindParam(":integrante", $datos['integrante'], PDO::PARAM_STR);
			// $sentencia->bindParam(":programa", $datos['programa'], PDO::PARAM_STR);
			// $sentencia->bindParam(":actividad", $datos['actividad'], PDO::PARAM_STR);
			return $sentencia->execute();
		}



	}



?>
