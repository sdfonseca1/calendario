<?php
	/* AUTOR:
	*  FECHA DE CREACIÓN:
	*  FECHA DE ÚLTIMA MODIFICACIÓN:
	*  DESCRIPCIÓN:
	*  ANOTACIONES:
	*/
	class Paginas{

		public function enlacesPaginasModelo($enlacesModelo){
			/*UTILIDAD: lista blanca de valores permitidos en la dirección.
			  PRECONDICION: recibe el nuevo valor de la variale accion de la dirección.
			  POSTCONDICIÓN: muestra la página a la que se quería acceder.
			*/
			if($enlacesModelo == "inicioSesion"){
			   	$modulo = "vista/modulos/inicioSesion.php";
			}else if ($enlacesModelo == "administrador") {
				$modulo = "vista/modulos/administrador.php";
			}else if ($enlacesModelo == "inicioSesionError") {
				$modulo = "vista/modulos/inicioSesion.php";
			}else if ($enlacesModelo == "cerrarSesion"){
				$modulo = "vista/modulos/cerrarSesion.php";
			}

			return $modulo;
		}

	}
?>