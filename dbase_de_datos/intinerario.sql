-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 02-11-2019 a las 11:59:02
-- Versión del servidor: 5.7.28
-- Versión de PHP: 7.2.19-0ubuntu0.18.04.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `intinerario`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actividad`
--

CREATE TABLE `actividad` (
  `clavePrograma` varchar(3) NOT NULL,
  `claveActividad` varchar(2) NOT NULL,
  `asignacion` varchar(100) DEFAULT NULL,
  `disponibilidad` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `actividad`
--

INSERT INTO `actividad` (`clavePrograma`, `claveActividad`, `asignacion`, `disponibilidad`) VALUES
('ACO', 'A', 'Acomodador/Microfonista', 1),
('ACO', 'B', 'Relevo de acomodador/microfonista', 0),
('AS', 'A', 'Abrir salón del Reino por lo menos 30 minutos antes de la reunión', 0),
('ATA', 'A', 'Presidente de la reunión entre semana', 0),
('ATA', 'B', 'Conductor del estudio Atalaya', 1),
('ATA', 'C', 'Lector del estudio Atalaya', 1),
('ATA', 'D', 'Oración final de la reunión de fin de semana', 0),
('AV', 'A', 'Preparar imágenes para la reunión', 0),
('AV', 'B', 'Preparar vídeos para la reunión', 0),
('AV', 'C', 'Preparar archivos de audio para la reunión', 0),
('AV', 'D', 'Proyectar imágenes/videos/audios durante la reunión', 0),
('AV', 'E', 'Plataforma durante la reunión', 0),
('AV', 'F', 'Proyectar textos bíblicos durante la reunión.', 0),
('CON', 'A', 'Orador en la congregación facultades', 1),
('CON', 'B', 'Sustituto del orador en la congregación facultades', 0),
('CON', 'C', 'Salir a dicursar a otra congregación(Ver programa para más detalles)', 1),
('LYM', 'A', 'Limpieza superficial del salón Reino', 0),
('LYM', 'B', 'Limpieza profunda del salón de Reino', 0),
('LYM', 'C', 'Aseo general del Salón de Reino', 0),
('NEC', 'A', 'Llevar de su casa al servicio', 1),
('NEC', 'B', 'Llevar de su casa a la reunión', 1),
('NEC', 'C', 'Llevar del servicio a su casa', 1),
('NEC', 'D', 'Llevar de la reunión a su casa', 1),
('NEC', 'E', 'Llevar de su casa a la asamblea', 1),
('NEC', 'F', 'Llevar de la asamblea a su casa', 1),
('PPU', 'B', 'Predicación pública', 0),
('SCA', 'A', 'Capitán', 0),
('VYM', 'A', 'Presidente de la reunión entre semana', 0),
('VYM', 'B', 'Tesoros del biblia', 0),
('VYM', 'C', 'Busquemos perlas escondidas', 0),
('VYM', 'DA', 'Lectura de la biblia (4 min o menos) - Sala a', 0),
('VYM', 'DB', 'Lectura de la biblia (4 minutos o menos) - Sala B', 0),
('VYM', 'EA', 'Primera conversación (discursante) - Sala A', 0),
('VYM', 'EB', 'Primera conversación (discursante) - Sala B', 0),
('VYM', 'FA', 'Primera revisita (discursante) - Sala A', 0),
('VYM', 'FB', 'Primera revisita ', 0),
('VYM', 'GA', 'Segunda revisita (discursante) - Sala A', 0),
('VYM', 'GB', 'Segunda revisita (discursante) - Sala B', 0),
('VYM', 'HA', 'Tercera revisita (discursante) - Sala A', 0),
('VYM', 'HB', 'Tercera revisita (discursante) - Sala B', 0),
('VYM', 'IA', 'Curso biblíco (discursante) - Sala A', 0),
('VYM', 'IB', 'Curso biblico (discursante) - Sala B', 0),
('VYM', 'JA', 'Primera conversación (ayudante) - Sala A', 0),
('VYM', 'JB', 'Primera conversación (ayudante) - Sala B', 0),
('VYM', 'KA', 'Primera revisita (ayudante) - Sala A', 0),
('VYM', 'KB', 'Primera revisita (ayudante) - Sala B', 0),
('VYM', 'LA', 'Segunda revisita (ayudante) - Sala A', 0),
('VYM', 'LB', 'Segunda revisita (ayudante) - Sala B', 0),
('VYM', 'MA', 'Tercera revisita (ayudante) - Sala A', 0),
('VYM', 'MB', 'Tercera revisita (ayudante) - Sala B', 0),
('VYM', 'NA', 'Curso bíblico (ayudante) - Sala A', 0),
('VYM', 'NB', 'Curso biblico (ayudante) Sala B', 1),
('VYM', 'OA', 'Discurso (6 mins o menos) - Sala A', 0),
('VYM', 'OB', 'Discurso (6 minutos o menos - Sala B)', 0),
('VYM', 'P1', 'Primera asignación en reunión nuestra vida cristiana', 0),
('VYM', 'P2', 'Segunda asignación en Reunión Nuestra vida cristina', 0),
('VYM', 'Q', 'Conductor del estudio biblico de la congregación', 0),
('VYM', 'R', 'Lector del estudio bíblico de la congregación', 1),
('VYM', 'S', 'Presidente de la sala B', 0),
('VYM', 'T', 'Oración final de la reunión de entre semana', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `integrante`
--

CREATE TABLE `integrante` (
  `clavePub` varchar(5) NOT NULL,
  `nombre` varchar(20) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `apellidoPaterno` varchar(15) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `apellidoMaterno` varchar(15) CHARACTER SET latin1 COLLATE latin1_spanish_ci NOT NULL,
  `sexo` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `integrante`
--

INSERT INTO `integrante` (`clavePub`, `nombre`, `apellidoPaterno`, `apellidoMaterno`, `sexo`) VALUES
('AAP0A', 'Abigail', 'Alvarez', 'Perez', 'M'),
('AGE0A', 'America', 'Garcia', 'Esquivel', 'M'),
('AMG0A', 'Alejandro', 'Moran', 'González', 'H'),
('AMS0A', 'Aarón', 'Martínez', 'Salas', 'H'),
('AMS0B', 'Amairani', 'Martínez', 'Salas', 'M'),
('ASM0A', 'Aarón', 'Martínez', 'Salas', 'H'),
('ASM0B', 'Amairani', 'Martínez', 'Salas', ''),
('ASM0C', 'Aurelia', 'Salas', 'de Martínez', 'M'),
('BAC0A', 'Belén', 'Álvarez', 'Chávez', 'M'),
('CAP0A', 'Catherine', 'Álvares', 'Pérez', 'M'),
('CGF0A', 'Carmen', 'García', 'de Feliciano', 'M'),
('CGH0A', 'Carmen', 'García', 'Posadas', 'M'),
('CRR0A', 'Carmen', 'Robles', '', 'M'),
('DMS0A', 'Dulce Anahí', 'Martínez', 'Salas', 'M'),
('EAG0A', 'Elizabeth Nalleli', 'Álvarez', 'Grande', 'M'),
('EGH0A', 'Elvira', 'García', 'de Hernández', 'M'),
('EGM0A', 'Estela', 'González', 'de Morán', 'M'),
('EMS0A', 'Emmanuel', 'Martínez', 'Salas', 'H'),
('FDC0A', 'Francisco', 'Durán', 'Camacho', 'H'),
('FMR0A', 'Felipe', 'Miranda', 'Rojas', 'H'),
('FRC0A', 'Fabiana', 'Rojas', 'Cruz', 'M'),
('FRH0A', 'Florencia', 'Rodríguez', 'de Hernández', 'M'),
('GAG0A', 'Getsemaní', 'Álvarez', 'Grande', 'H'),
('GAM0A', 'Gonzalo', 'Álvarez', 'Martínez', 'H'),
('GRR0A', 'Guadalupe', 'Rebollar', '', 'M'),
('HAM0A', 'Hilario', 'Álvarez', 'Martínez', 'H'),
('ILR0A', 'Irma', 'Lara', 'de Robles', 'M'),
('JAM0A', 'Javier', 'Álvarez', 'Martínez', 'H'),
('JGA0A', 'Josefina', 'Álvarez', 'de Hernández', 'M'),
('JGP0A', 'Juan', 'García', 'Posada', 'H'),
('JMG0A', 'Jesús', 'Martínez', 'Galindo', 'H'),
('JMM0A', 'Josué', 'Miranda', 'Martínez', 'H'),
('JPA0A', 'Juana', 'Pérez', 'de Álvarez', 'M'),
('JSG0A', 'Javier', 'Salas', 'García', 'H'),
('JSS0A', 'Javier', 'Salas', 'Soto', 'H'),
('KMM0A', 'Kimberly', 'Miranda', 'Martínez', 'M'),
('LPR0A', 'Luis', 'Pérez', 'Reyes', 'H'),
('MCA0A', 'María', 'Cruz', 'Atilano', 'M'),
('MCA0B', 'María Isabel', 'Chávez', 'de Álvarez', 'M'),
('MEA0A', 'Maria Elena', 'Álvarez', 'Martínez', 'M'),
('MGH0A', 'María Félix', 'García', 'Hernández', 'M'),
('MGS0A', 'Martha', 'García', 'de Salas', 'M'),
('MLM0A', 'María Luisa', 'Mejía', 'de Martínez', 'M'),
('MMG0A', 'Miguel', 'Morán', 'González', 'H'),
('MMM0A', 'Mónica', 'Martínez', 'Mejía', 'M'),
('MMR0A', 'Marlen', 'Munguía', 'Martínez', 'M'),
('MPA0A', 'Mario', 'Perea', 'Arellano', 'M'),
('MPA0B', 'Miriam', 'Perea', 'Aviles', 'M'),
('MPE0A', 'Maria de la Paz', 'Esquivel', '', 'M'),
('PPA0A', 'Pedro', 'Pablo', 'Abellán', 'H');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `integrante_actividad`
--

CREATE TABLE `integrante_actividad` (
  `claveIntegrante` varchar(5) DEFAULT NULL,
  `clavePrograma` varchar(3) DEFAULT NULL,
  `claveActividad` varchar(2) DEFAULT NULL,
  `ultimaAsignacion` date DEFAULT NULL,
  `disponibilidad` bit(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `integrante_actividad`
--

INSERT INTO `integrante_actividad` (`claveIntegrante`, `clavePrograma`, `claveActividad`, `ultimaAsignacion`, `disponibilidad`) VALUES
('AAP0A', 'ACO', 'A', NULL, b'1'),
('BAC0A', 'ACO', 'A', '2019-10-10', b'1'),
('ASM0C', 'ACO', 'A', '2019-08-10', b'1'),
('CRR0A', 'ACO', 'A', '2019-03-10', b'1'),
('HAM0A', 'ACO', 'A', '2019-11-02', b'0');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `intinerario`
--

CREATE TABLE `intinerario` (
  `cveCalendario` int(10) UNSIGNED NOT NULL,
  `fecha` date DEFAULT NULL,
  `clavePub` varchar(5) DEFAULT NULL,
  `clavePrograma` varchar(3) DEFAULT NULL,
  `claveActividad` varchar(2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `intinerario`
--

INSERT INTO `intinerario` (`cveCalendario`, `fecha`, `clavePub`, `clavePrograma`, `claveActividad`) VALUES
(11, '2019-11-01', 'AAP0A', 'ACO', 'A'),
(12, '2019-11-02', 'HAM0A', 'ACO', 'A');

--
-- Disparadores `intinerario`
--
DELIMITER $$
CREATE TRIGGER `ACTUALIZAR_DISPONIBILIDAD` AFTER INSERT ON `intinerario` FOR EACH ROW BEGIN
		UPDATE integrante_actividad SET ultimaAsignacion = NEW.fecha WHERE NEW.clavePub=integrante_actividad.claveIntegrante
		AND NEW.clavePrograma=integrante_actividad.clavePrograma AND NEW.claveActividad=integrante_actividad.claveActividad;
		UPDATE integrante_actividad SET disponibilidad = 0 WHERE NEW.clavePub=integrante_actividad.claveIntegrante
		AND NEW.clavePrograma=integrante_actividad.clavePrograma AND NEW.claveActividad=integrante_actividad.claveActividad;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programa`
--

CREATE TABLE `programa` (
  `clavePrograma` varchar(3) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `programa`
--

INSERT INTO `programa` (`clavePrograma`, `nombre`) VALUES
('ACO', 'Acomodadores'),
('AS', 'Abrir salón del reino'),
('ATA', 'Atalaya'),
('AV', 'Audiovisual'),
('CON', 'Conferencias públicas'),
('LYM', 'Limpieza'),
('NEC', 'Necesidad especial'),
('PPU', 'Predicación pública'),
('SCA', 'Servicio del campo'),
('VYM', 'Reunión entre semana');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `clave` varchar(10) NOT NULL,
  `contrasena` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`clave`, `contrasena`) VALUES
('1113637', 'qwer1234');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD PRIMARY KEY (`clavePrograma`,`claveActividad`);

--
-- Indices de la tabla `integrante`
--
ALTER TABLE `integrante`
  ADD PRIMARY KEY (`clavePub`);

--
-- Indices de la tabla `integrante_actividad`
--
ALTER TABLE `integrante_actividad`
  ADD KEY `claveIntegrante` (`claveIntegrante`),
  ADD KEY `clavePrograma` (`clavePrograma`,`claveActividad`);

--
-- Indices de la tabla `intinerario`
--
ALTER TABLE `intinerario`
  ADD PRIMARY KEY (`cveCalendario`),
  ADD KEY `clavePrograma` (`clavePrograma`,`claveActividad`),
  ADD KEY `clavePub` (`clavePub`);

--
-- Indices de la tabla `programa`
--
ALTER TABLE `programa`
  ADD PRIMARY KEY (`clavePrograma`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`clave`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `intinerario`
--
ALTER TABLE `intinerario`
  MODIFY `cveCalendario` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actividad`
--
ALTER TABLE `actividad`
  ADD CONSTRAINT `actividad_ibfk_1` FOREIGN KEY (`clavePrograma`) REFERENCES `programa` (`clavePrograma`);

--
-- Filtros para la tabla `integrante_actividad`
--
ALTER TABLE `integrante_actividad`
  ADD CONSTRAINT `integrante_actividad_ibfk_1` FOREIGN KEY (`claveIntegrante`) REFERENCES `integrante` (`clavePub`),
  ADD CONSTRAINT `integrante_actividad_ibfk_2` FOREIGN KEY (`clavePrograma`,`claveActividad`) REFERENCES `actividad` (`clavePrograma`, `claveActividad`);

--
-- Filtros para la tabla `intinerario`
--
ALTER TABLE `intinerario`
  ADD CONSTRAINT `intinerario_ibfk_1` FOREIGN KEY (`clavePrograma`,`claveActividad`) REFERENCES `actividad` (`clavePrograma`, `claveActividad`),
  ADD CONSTRAINT `intinerario_ibfk_2` FOREIGN KEY (`clavePub`) REFERENCES `integrante` (`clavePub`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
