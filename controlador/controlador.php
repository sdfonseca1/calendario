<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 5 Agosto 2019
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: 
*  ANOTACIONES:
*/


	class Controlador{

		public function pagina(){
			/*UTILIDAD: hace un llamado a la plantilla.
			  PRECONDICION: 
			  POSTCONDICIÓN: muestra la plantilla.
			*/
			include("vista/plantilla.php");
		}

		public function enlacesPaginasControlador(){
			/*UTILIDAD: genera la nueva página a la que se desea ir de la opción del menú.
			  PRECONDICION: 
			  POSTCONDICIÓN: la nueva página a la que se desea ir de la opción del menú es mostrada.
			*/
			if(isset($_GET['accion'])){	
				$enlaces = $_GET['accion'];
			}else{
				$enlaces = "index";
			}
			$respuesta = Paginas::enlacesPaginasModelo($enlaces);
			include($respuesta);
		}

		public function ingresoUsuarioControlador(){
			/*UTILIDAD: recibe los datos del formulario del usaurio.
			  PRECONDICION: los datos deben ser validados en el formulario.
			  POSTCONDICIÓN: recibe falso o verdadero delmodelo si los datos del formulario existen en la BD.
			*/
		
			if (isset($_POST["clave"])) {
				$datosControlador = array("clave" => $_POST["clave"],
										  "contrasena" => $_POST["contrasena"]);
				$respuesta = Modelo::ingresoUsuarioModelo("usuario", $datosControlador);
				
				if ($respuesta["clave"] == $_POST["clave"] && $respuesta["contrasena"] == $_POST["contrasena"]){
					session_start();
					$_SESSION["usuario"] = $respuesta['clave'];
					header("location:index.php?accion=administrador");
				}
				else{
					header("location:index.php?accion=inicioSesionError");
				}
			}
		}

		public function pruebaControlador(){
			/*UTILIDAD: regresa los datos de los integrantes.
			  PRECONDICION: debe haber registros en la base de datos.
			  POSTCONDICIÓN: regresa los datos de los integrantes
			*/
			$respuesta = Modelo::pruebaModelo("integrante");
			return $respuesta;
		}

		//INTEGRANTES
		public function obtenerSeleccionIntegrantesControlador(){
			/*UTILIDAD: regresa los datos de los integrantes.
			  PRECONDICION: debe haber registros en la base de datos.
			  POSTCONDICIÓN: regresa los datos de los integrantes
			*/
			$respuesta = Modelo::obtenerSeleccionIntegrantesModelo("integrante");
			return $respuesta;
		}

		public function reporteAsignacionIntegranteControlador(){
			/*UTILIDAD: regresa los datos de los integrantes.
			  PRECONDICION: debe haber registros en la base de datos.
			  POSTCONDICIÓN: regresa los datos de los integrantes
			*/
			$respuesta = Modelo::reporteAsignacionIntegranteModelo("integrante_actividad");
			return $respuesta;
		}

		public function obtenerSeleccionIntegrantesControlador2(){
			/*UTILIDAD: regresa los datos de los integrantes.
			  PRECONDICION: debe haber registros en la base de datos.
			  POSTCONDICIÓN: regresa los datos de los integrantes
			*/
			$respuesta = Modelo::obtenerSeleccionIntegrantesModelo2("integrante");
			return $respuesta;
		}

		public function obtenerIntegrantesControlador($num_reg){
			/*UTILIDAD: muestra los datos de los integrantes..
			  PRECONDICION: los datos deben ser validados en el formulario.
			  POSTCONDICIÓN: recibe falso o verdadero delmodelo si los datos del formulario existen en la BD.
			*/
			$respuesta = Modelo::obtenerIntegrantesModelo("integrante", $num_reg);
			return $respuesta;
		}

		public function obtenerIntegrantesRegistradosControlador(){
			/*UTILIDAD: muestra los datos de los integrantes..
			  PRECONDICION: los datos deben ser validados en el formulario.
			  POSTCONDICIÓN: recibe falso o verdadero delmodelo si los datos del formulario existen en la BD.
			*/
			$respuesta = Modelo::obtenerIntegrantesRegistradosModelo("integrante");
			return $respuesta;
		}

		public function obtenerIntegranteControlador($clave){
			/*UTILIDAD: devuelve los datos de un integrante.
			  PRECONDICION: recibe la clave del integrante a buscar.
			  POSTCONDICIÓN: recibe del modelo los datos del integrante buscado.
			*/
			// var_dump($clave);
			$respuesta = Modelo::obtenerIntegranteModelo("integrante", $clave);
			return $respuesta;
		}

		public function actualizarIntegranteControlador($datos){
			/*UTILIDAD: actualiza los datos de un integrante.
			  PRECONDICION: recibe los datos del integrante a buscar.
			  POSTCONDICIÓN: recibe falso o verdadero si actualizaron los datos del integrante.
			*/
			$respuesta = Modelo::actualizarIntegranteModelo("integrante", $datos);
			return $respuesta;
		}

		public function eliminarIntegranteControlador($clave){
			/*UTILIDAD: elimina los datos de un integrante.
			  PRECONDICION: recibe la clave del integrante a buscar.
			  POSTCONDICIÓN: recibe falso o verdadero si se eliminaron los datos del integrante.
			*/
			$respuesta = Modelo::eliminarIntegranteModelo("integrante", $clave);
			return $respuesta;
		}

		public function existeAsignacionControlador($datos){
			/*UTILIDAD: elimina los datos de un integrante.
			  PRECONDICION: recibe la clave del integrante a buscar.
			  POSTCONDICIÓN: recibe falso o verdadero si se eliminaron los datos del integrante.
			*/
			$respuesta = Modelo::existeAsignacionModelo("integrante_actividad", $datos);
			return $respuesta;
		}

		public function buscarClaveControlador($clave){
			/*UTILIDAD: muestra los datos de los integrantes..
			  PRECONDICION: los datos deben ser validados en el formulario.
			  POSTCONDICIÓN: recibe falso o verdadero delmodelo si los datos del formulario existen en la BD.
			*/
			$respuesta = Modelo::buscarClaveModelo('integrante', $clave);
			return $respuesta;
		}

		public function registrarIntegranteControlador($datos){
			/*UTILIDAD: registra los datos de un integrante.
			  PRECONDICION: los datos deben ser validados en el formulario.
			  POSTCONDICIÓN: recibe falso o verdadero del modelo si los datos fueron registrados o no.
			*/
			$respuesta = Modelo::registrarIntegranteModelo('integrante', $datos);
			// var_dump($respuesta);
			return $respuesta;
		}

		// PROGRAMAS
		public function obtenerProgramasControlador($num_reg){
			/*UTILIDAD: muestra los datos de los integrantes..
			  PRECONDICION: los datos deben ser validados en el formulario.
			  POSTCONDICIÓN: recibe falso o verdadero delmodelo si los datos del formulario existen en la BD.
			*/
			$respuesta = Modelo::obtenerProgramasModelo("programa", $num_reg);
			return $respuesta;
		}

		public function buscarClaveProgramaControlador($clave){
			/*UTILIDAD: busca la clave de un programa para saber si ya existe.
			  PRECONDICION: los datos deben ser validados en el formulario.
			  POSTCONDICIÓN: recibe falso o verdadero del modelo si la clave del programa existe en la BD.
			*/
			$respuesta = Modelo::buscarClaveProgramaModelo('programa', $clave);
			return $respuesta;
		}

		public function registrarProgramaControlador($datos){
			/*UTILIDAD: registra los datos de un programa.
			  PRECONDICION: los datos deben ser validados en el formulario.
			  POSTCONDICIÓN: recibe falso o verdadero del modelo si los datos fueron registrados o no.
			*/
			$respuesta = Modelo::registrarProgramaModelo('programa', $datos);
			// var_dump($respuesta);
			return $respuesta;
		}

		public function obtenerProgramaControlador($clave){
			/*UTILIDAD: devuelve los datos de un programa.
			  PRECONDICION: recibe la clave del programa a buscar.
			  POSTCONDICIÓN: recibe del modelo los datos del programa buscado.
			*/
			// var_dump($clave);
			$respuesta = Modelo::obtenerProgramaModelo("programa", $clave);
			return $respuesta;
		}

		public function actualizarProgramaControlador($datos){
			/*UTILIDAD: actualiza los datos de un programa.
			  PRECONDICION: recibe los datos del programa a buscar.
			  POSTCONDICIÓN: recibe falso o verdadero si actualizaron los datos del programa.
			*/
			$respuesta = Modelo::actualizarProgramaModelo("programa", $datos);
			return $respuesta;
		}

		public function eliminarProgramaControlador($clave){
			/*UTILIDAD: elimina los datos de un programa.
			  PRECONDICION: recibe la clave del programa a buscar.
			  POSTCONDICIÓN: recibe falso o verdadero si se eliminaron los datos del programa.
			*/
			$respuesta = Modelo::eliminarProgramaModelo("programa", $clave);
			return $respuesta;
		}

		public function obtenerDespliegueProgramasControlador(){
			/*UTILIDAD: obtiene clave y descripción del programa.
			  PRECONDICION:
			  POSTCONDICIÓN:
			*/
			$respuesta = Modelo::obtenerDespliegueProgramasModelo("programa");
			return $respuesta;
		}
		// ACTIVIDADES
		public function obtenerDespliegueActividadesControlador($programa){
			/*UTILIDAD: obtiene clave y descripción del programa.
			  PRECONDICION:
			  POSTCONDICIÓN:
			*/
			$respuesta = Modelo::obtenerDespliegueActividadesModelo("actividad", $programa);
			// var_dump($respuesta);
			return $respuesta;
		}

		public function obtenerDespliegueActividadesControlador2(){
			/*UTILIDAD: obtiene clave y descripción del programa.
			  PRECONDICION:
			  POSTCONDICIÓN:
			*/
			$respuesta = Modelo::obtenerDespliegueActividadesModelo2("actividad");
			// var_dump($respuesta);
			return $respuesta;
		}

		public function obtenerActividadesControlador($num_reg){
			/*UTILIDAD: muestra los datos de los integrantes..
			  PRECONDICION: los datos deben ser validados en el formulario.
			  POSTCONDICIÓN: recibe falso o verdadero delmodelo si los datos del formulario existen en la BD.
			*/
			$respuesta = Modelo::obtenerActividadesModelo("actividad", $num_reg);
			return $respuesta;
		}

		public function buscarClaveActividadControlador($clave_prog, $clave_act){
			/*UTILIDAD: busca la clave de un programa para saber si ya existe.
			  PRECONDICION: los datos deben ser validados en el formulario.
			  POSTCONDICIÓN: recibe falso o verdadero del modelo si la clave del programa existe en la BD.
			*/
			$respuesta = Modelo::buscarClaveActividadModelo('actividad', $clave_prog, $clave_act);
			return $respuesta;
		}

		public function registrarActividadControlador($datos){
			/*UTILIDAD: registra los datos de una actividad.
			  PRECONDICION: los datos deben ser validados en el formulario.
			  POSTCONDICIÓN: recibe falso o verdadero del modelo si los datos fueron registrados o no.
			*/
			$respuesta = Modelo::registrarActividadModelo('actividad', $datos);
			// var_dump($respuesta);
			return $respuesta;
		}

		public function obtenerActividadControlador($datos_acti){
			/*UTILIDAD: obtiene los datos de una actividad.
			  PRECONDICION: recibe la clave de una actividad.
			  POSTCONDICIÓN: devuelve los datos si existen en la base de datos.
			*/
			$respuesta = Modelo::obtenerActividadModelo("actividad", $datos_acti["programa"], $datos_acti["actividad"]);
			return $respuesta;
		}

		public function actualizarActividadControlador($datos){
			/*UTILIDAD: actualiza los datos de un actividad.
			  PRECONDICION: recibe los datos del actividad a buscar.
			  POSTCONDICIÓN: recibe falso o verdadero si actualizaron los datos del actividad.
			*/
			$respuesta = Modelo::actualizarActividadModelo("actividad", $datos);
			return $respuesta;
		}

		public function eliminarActividadControlador($datos){
			/*UTILIDAD: elimina los datos de un actividad.
			  PRECONDICION: recibe la clave del actividad a buscar.
			  POSTCONDICIÓN: recibe falso o verdadero si se eliminaron los datos del actividad.
			*/
			$respuesta = Modelo::eliminarActividadModelo("actividad", $datos['prog'], $datos['actividad']);
			return $respuesta;
		}

		public function asignarActividadIntegranteControlador($datos){
			/*UTILIDAD: 
			  PRECONDICION:
			  POSTCONDICIÓN: recibe falso o verdadero si actualizaron los datos del actividad.
			*/
			$respuesta = Modelo::asignarActividadIntegranteModelo("integrante_actividad", $datos);
			return $respuesta;
		}

		public function crearFechaAsignacionControlador($datos){
			/*UTILIDAD: 
			  PRECONDICION:
			  POSTCONDICIÓN: recibe falso o verdadero si actualizaron los datos del actividad.
			*/
			$respuesta = Modelo::crearFechaAsignacionModelo("intinerario", $datos);
			return $respuesta;
		}


		public function eliminarAsignacionIntegranteControlador($datos){
			/*UTILIDAD: 
			  PRECONDICION:
			  POSTCONDICIÓN: recibe falso o verdadero si actualizaron los datos del actividad.
			*/
			$respuesta = Modelo::eliminarAsignacionIntegranteModelo("integrante_actividad", $datos);
			return $respuesta;
		}

		public function mostrarIntegrantesPorActividadControlador(){
			/** UTILIDAD:
			 ** PRECONDICION:
			 ** POSTCONDICIÓN:
			**/
			$respuesta = Modelo::mostrarIntegrantesPorActividadModelo("integrante_actividad");
			return $respuesta;
		}

		public function existenRegistrosActIntControlador(){
			/*UTILIDAD: 
			  PRECONDICION:
			  POSTCONDICIÓN: recibe falso o verdadero si actualizaron los datos del actividad.
			*/
			$respuesta = Modelo::existenRegistrosActIntModelo("integrante_actividad");
			return $respuesta[0];
		}

		public function mostrarIntegrantesDisponiblesControlador($datos){
			/*UTILIDAD: muestra los datos de los integrantes..
			  PRECONDICION: los datos deben ser validados en el formulario.
			  POSTCONDICIÓN: recibe falso o verdadero delmodelo si los datos del formulario existen en la BD.
			*/
			$respuesta = Modelo::mostrarIntegrantesDisponiblesModelo("integrante_actividad", $datos);
			// var_dump($respuesta);
			return $respuesta;
		}

		public function reporteFechaControlador($fecha){
			/*UTILIDAD:
			  PRECONDICION:
			  POSTCONDICIÓN:
			*/
			$respuesta = Modelo::reporteFechaModelo("intinerario", $fecha);
			// var_dump($respuesta);
			return $respuesta;
		}

		function latin1ToUTF8($datos){
	    	if (is_string($datos)) {
	        	return utf8_encode($datos);
			}elseif (is_array($datos)) {
	        	$ret = [];
	        	foreach ($datos as $i => $d) $ret[ $i ] = self::latin1ToUTF8($d);
				return $ret;
	    	}elseif (is_object($datos)) {
	        	foreach ($datos as $i => $d) $datos->$i = self::latin1ToUTF8($d);
				return $datos;
	      	}else {
	        	return $datos;
	      	}
    	}
	}


?>