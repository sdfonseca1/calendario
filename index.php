<?php
/* AUTOR: SDFonseca1
*  FECHA DE CREACIÓN: 23 AGO 2019
*  FECHA DE ÚLTIMA MODIFICACIÓN:
*  DESCRIPCIÓN: index del sistema.
*  ANOTACIONES:
*/
	require_once "controlador/controlador.php";
	require_once "modelo/modelo.php";
	require_once("modelo/enlaces.php");

	$pagina = new Controlador();
	$pagina->pagina();
?>