$(document).ready(function(){

//***********************************INTEGRANTES****************************************//
	//Muestra la lista de integrantes al hacer click en el nombre del acordeón.
	$("#integrantes").one('click', function(){
		mostrarListaIntegrantes(0);
	});




	//local supuestamente
	//Muestra contenido paginado
	$(document).on('click', 'a.page-link', function(event) {
		var href = $(this).attr('href');
	    var pagina = href.split("=")
	    console.log(pagina[1]);
	    $("contenido-actividades").empty();
	    mostrarListaIntegrantes(pagina[1]);
	});

	$("#btnCerrarRegistro").click(function(){
		limpiarFormulario("formularioRegIntegrante");
	})

	// Revisamos la clave una vez que el campo clave pierde el foco...
	$("input[name=campoClave]").blur(function(){
		var clave = this.value;
		buscarClaveIntegrante(clave);
	});


	$("#btnAgregarInt").click(function(){
		agregarIntegrante();
	});

	$("#btnActualizarInt").click(function(){
		obtenerDatosActualizarIntegrante();
	});

	$("#btnEliminarInt").click(function(){
		var claveEliminar = $(':input[name="campoEliClave"]').val();
		eliminarIntegrante(claveEliminar.substring(0,5));
	});

	// Llena el formulario para eliminar datos...
	$(document).on('click', 'a.btnEli', function(event) {
	    var claveEliminar = $(this).attr('id');
	    llenarModalEliminacionIntegrante(claveEliminar.substring(0,5));
	});

	// Llenar formulario para editar integrante.
	$(document).on('click', 'a.btn', function(event) {
	    var claveEditar = $(this).attr('id');
	    llenarModalIntegrante(claveEditar);
	});


//***********************************PROGRAMAS****************************************//
	$("#programas").one('click', function(){
		mostrarListaProgramas(0);
	});

	// Clave del programa.
	$("input[name=campoClavePrograma]").blur(function(){
		var clave = this.value;
		buscarClavePrograma(clave);
	});

	// Agregar programa a la base de datos.
	$("#btnAgregarPrograma").click(function(){
		agregarPrograma();
	});

	// Función al boton editar programa
	$(document).on('click', 'a.btnEditarPrograma', function(event) {
	    var claveEditar = $(this).attr('id');
	    llenarModalEditarPrograma(claveEditar.substring(0,3));
	});

	// Función al boton eliminar programa
	$(document).on('click', 'a.btnEliminarPrograma', function(event) {
	    var claveEditar = $(this).attr('id');
	    llenarModalEliminarPrograma(claveEditar.substring(0,3));
	});

	// Función al botón eliminar programa del modal
	$("#btnEliminarPrograma").click(function(){
		var claveEliminar = $(':input[name="campoEliminarClavePrograma"]').val();
		eliminarPrograma(claveEliminar.substring(0,5));
	});

	$("#btnActualizarPrograma").click(function(){
		obtenerDatosActualizarPrograma();
	});

	$(document).on('click', 'a.page-link', function(event) {
		// alert("click");
		var href = $(this).attr('href');
	    var pagina = href.split("=")
	    // console.log(pagina[1]);
	    $("contenido-programas").empty();
	    mostrarListaProgramas(pagina[1]);
	});

//***********************************ACTIVIDADES****************************************//
	$("#actividades").one('click', function(){
		mostrarListaActividades(0);
	});

	//Muestra contenido paginado
	$(document).on('click', 'a.page-link', function(event) {
		var href = $(this).attr('href');
	    var pagina = href.split("=")
	    console.log(pagina[1]);
	    $("contenido-actividades").empty();
	    mostrarListaActividades(pagina[1]);
	});

	$(document).on('click', 'a.agregarActividad', function(event) {
		limpiarFormulario("formularioActividad");
	    despliegueProgramas("campoAgregaPrograma", "campoRegistroActividad");
	});

	//Obtiene valor de la selección
	$('select').on('change', function (e) {
		    var opcionSeleccionada = $("option:selected", this);
		    var valorSeleccion = this.value;
		    // console.log(optionSeleccionada + " - " + valorSeleccion);
		    if (valorSeleccion != "") {
				$(":input[name=campoRegistroActividad]").prop('disabled', false);
			}else{
				$(":input[name=campoRegistroActividad]").prop('disabled', true);
				$("#btnAgregarPrograma").prop('disabled', true);
			}
	});

	// Busca que la clave de la actividad no esté registrada.
	$("input[name=campoRegistroActividad").blur(function(){
		var clave_programa = $('select[name=campoSeleccionPrograma]').val();
		var clave_actividad = this.value;
		buscarClaveActividad(clave_programa, clave_actividad);
	});

	// Botón agregar actividad.
	$("#btnAgregarActividad").click(function(){
		// console.log("Hola, funciona el botón")
		agregarActividad();
	});

	// Llenar formulario para editar actividad.
	$(document).on('click', 'a.btnEditarActividad', function(event) {
	    var claveEditar = $(this).attr('id');
	    console.log(claveEditar.substring(0,3));
	    console.log(claveEditar.substring(3,4));
	    llenarModalEditarActividad(claveEditar.substring(0,3),claveEditar.substring(3,4));
	});

	$("#btnActualizarActividad").click(function(){
		obtenerDatosActualizarActividad();
	});

	// Función al boton eliminar programa
	$(document).on('click', 'a.btnEliminarActividad', function(event) {
	    var claveEditar = $(this).attr('id');
	    // console.log(claveEditar.substring(0,4));
	    llenarModalEliminarActividad(claveEditar.substring(0,3), claveEditar.substring(3,4));
	});

	$("#btnEliminarActividad").click(function(){
		var clave_programa = $(':input[name="campoEliminarSeleccionPrograma"]').val();
		var clave_actividad = $(':input[name="campoEliminarClaveActividad"]').val();
		eliminarActividad(clave_programa, clave_actividad);
	});

//***********************************ASIGNACIÓN****************************************//
	$("#fechaAsignacion").change(function(event) {
		if ($(this).val() != "") {
			$("#select-programas").removeAttr('disabled');
		}
	});

	$("#asignarActividades").one('click', function(){
		$("#select-programas").attr('disabled', '');
		mostrarSeleccionListaPrograma();
		$('#selecIntegrante').attr('disabled', '');
		// var fecha = new Date();
		// $("#fechaAsignacion").attr('min',fecha.getYear()+"-"+fecha.getMonth()+"-"+fecha.getDay());
		// $("#fechaAsignacion").attr('value',fecha.getYear()+"-"+fecha.getMonth()+"-"+fecha.getDay());
		// mostrarIntegrantesDisponibles();
	});

	$("#select-programas").change(function(){
    	var prog = $('select[id=select-programas]').val();
    	$("#select-actividades").empty();
    	$("#select-actividades").removeAttr('disabled');
    	$("#select-actividades").append("<option>Elija una actividad</option>");
    	mostrarSeleccionListaActividades(prog);
	});

	$("#select-actividades").change(function(){
    	var prog = $('select[id=select-programas]').val();
    	var acti = $('select[id=select-actividades]').val();
    	var fecha = $("#fechaReporte").val();
    	$("#selecIntegrante").empty();
    	$("#selecIntegrante").append('<option>Elija un integrante</option>');
    	$('#selecIntegrante').removeAttr('disabled');
    	mostrarIntegrantesDisponibles(fecha, prog, acti);
	});

	$("#btnReporteFecha").click(function(event) {
		var fecha = $("#fechaReporte").val();
		obtenerReporteFecha(fecha);
	});

	
/**************************************************************************/
	
	$("#asignarActInt").one('click', function(){
		existenRegistrosActividadIntegrante();
		obtenerSeleccionIntegrantes2();
		mostrarSeleccionListaActividades2();
	});

	$("#btnAsignar").click(function(event) {
		var inte = $('select[id=selecIntegranteAct]').val();
		var prog = $('select[id=select-actividadesProg]').val();
		if (inte != "" && prog != "") {
			var tabla = prog.split("-");
			existeAsignacion(inte, tabla[0], tabla[1]);
		}else{
			alert("Debe elegir una opción de los SELECT.");
		}
		
		
	});

	$(document).on('click', 'a.btnEliminarIA', function(event) {
		var datos = $(this).attr('id');
		datos = datos.split("-");
		eliminarAsignacionIntegrante(datos[0], datos[1], datos[2]);
	});

	$("#btnCrearAsignacion").click(function(event) {
		$("reporte-detallado").empty();
		if (camposVacios()){
			alert("Todos los campos deben estar seleccionados.");
		}
	});

	$("#tarjeta").click(function(){
		alert("Haz hecho click")
		pruebaLista();
	});


});




function pruebaLista(){
			var datos = {
				funcion: 'prueba',
			};
			$.ajax({
				type: 'POST',
				url: "vista/modulos/ajax.php",
				data: datos,
				dataType: 'json',
				encode: false,
				success: function(respuesta){
					$('#lista-pub').empty();
					console.log(respuesta);
					respuesta.forEach(function(e){
						$('#lista-pub').append('<option value="'+e[0]+'">'+e[1]+" "+e[2]+" "+e[3]+'</option>')
					})
				},
				error: function(){
					console.log("Error.");
				}
			})
			.fail( function( jqXHR, textStatus, errorThrown ) {
			    console.log(textStatus + errorThrown);
			});
	}


/*********************Funciones de integrantes*******************/
	function llenarModalIntegrante(clave){
		/*UTILIDAD: escribe datos en el formulario integrante.*/
		var integrante = obtenerDatosIntegrante(clave);
	}

	function llenarModalEliminacionIntegrante(clave){
		/*UTILIDAD: escribe datos en el formulario integrante.*/
		var integrante = obtenerDatosEliminarIntegrante(clave);
	}

	function obtenerDatosIntegrante(clave){
		/*UTILIDAD: obtiene los datos de un integrante.*/
		datos = {
			funcion:'obtenerIntegrante',
			clave:clave
		};
		$.ajax({
				type: 'POST',
				url: "vista/modulos/ajax.php",
				data: datos,
				dataType: 'json',
				encode: false,
				success: function(e){
					$(':input[name="campoActClave"]').attr("value", e[0]);
					$(':input[name="campoActClave"]').attr("disabled", "");
					$(':input[name="campoActNombre"]').attr("value", (e[1]));
					$(':input[name="campoActApePat"]').attr("value", (e[2]));
					$(':input[name="campoActApeMat"]').attr("value", (e[3]));
					$('select[name="campoActSexo"]').val(e[4]);			
				},
				error: function(){
					console.log("Error.");
				}
			})
			.fail( function( jqXHR, textStatus, errorThrown ) {
			    console.log(textStatus + errorThrown);
			});
	}

	function obtenerDatosEliminarIntegrante(clave){
		/*UTILIDAD: obtiene los datos de un integrante.*/
		datos = {
			funcion:'obtenerIntegrante',
			clave:clave
		};
		$.ajax({
				type: 'POST',
				url: "vista/modulos/ajax.php",
				data: datos,
				dataType: 'json',
				encode: false,
				success: function(e){
					// console.log(e);
					$(':input[name="campoEliClave"]').attr("value", e[0]);
					$(':input[name="campoEliClave"]').attr("readonly", "readonly");
					$(':input[name="campoEliNombre"]').attr("value", (e[1]));
					$(':input[name="campoEliNombre"]').attr("readonly", "readonly");
					$(':input[name="campoEliApePat"]').attr("value", (e[2]));
					$(':input[name="campoEliApePat"]').attr("readonly", "readonly");
					$(':input[name="campoEliApeMat"]').attr("value", (e[3]));
					$(':input[name="campoEliApeMat"]').attr("readonly", "readonly");
					$('select[name="campoEliSexo"]').val(e[4]);
					$('select[name="campoEliSexo"]').attr('disabled',true);		
				},
				error: function(){
					console.log("Error.");
				}
			})
			.fail( function( jqXHR, textStatus, errorThrown ) {
			    console.log(textStatus + errorThrown);
			});
	}

	function obtenerDatosActualizarIntegrante(){
		/*UTILIDAD: actualiza los datos del integrante previamente registrado.*/
		var campoClave = document.getElementsByName("campoActClave")[0].value;
		var campoNombre = document.getElementsByName("campoActNombre")[0].value;
		var campoApePat = document.getElementsByName("campoActApePat")[0].value;
		var campoApeMat = document.getElementsByName("campoActApeMat")[0].value;
		var campoSexo = $("select[name='campoActSexo']").val();
		campoClave = campoClave.toUpperCase();
		var datos = {
			funcion: 'actualizarIntegrante',
			clave: campoClave,
			nombre: campoNombre,
			paterno: campoApePat,
			materno: campoApeMat,
			sexo: campoSexo,
		};
		actualizarIntegrante(datos);
	}

	function actualizarIntegrante(datos){
		/*UTILIDAD: actualiza datos de un integrante mediante AJAX.*/
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'text',
			encode: false,
			success: function(respuesta){
				alert("Datos actualizados correctamente...");
				$("#contenido-tabla").empty();
				mostrarListaIntegrantes(0);
			},
			error: function(){
				alert("No se ha podido actualizar.. intente nuevamente.");
			}
		});
	}

	function agregarIntegrante(){
		/*UTILIDAD: obtiene los campos del formulario integrante e inicia el registro del mismo.*/
		console.log("Agregar integrante");
		var campoClave = document.getElementsByName("campoClave")[0].value;
		var campoNombre = document.getElementsByName("campoNombre")[0].value;
		var campoApePat = document.getElementsByName("campoApePat")[0].value;
		var campoApeMat = document.getElementsByName("campoApeMat")[0].value;
		var campoSexo = $("select[name='campoSexo']").val();
		campoClave = campoClave.toUpperCase();
		var datos = {
			funcion: 'registrarIntegrante',
			clave: campoClave,
			nombre: campoNombre,
			paterno: campoApePat,
			materno: campoApeMat,
			sexo: campoSexo,
		};
		registrarIntegrante(datos);
	}

	function mostrarListaIntegrantes(pagina){
			var datos = {
				funcion: 'obtenerIntegrantes',
				num_reg: pagina, 
			};
			$.ajax({
				type: 'POST',
				url: "vista/modulos/ajax.php",
				data: datos,
				dataType: 'json',
				encode: false,
				success: function(respuesta){
					var i = 0;
					$("#contenido-tabla").empty();
					if (pagina == 0 || pagina == 1) {
						i = 1;
					}else if(pagina >= 2){
						i = (pagina*10)-9;
					}
					respuesta.forEach(function(e){
						$("#contenido-tabla").append("<tr>"+"<td>"+(i++)+"</td>"+"<td>"+e[0]+"</td>"+"<td>"+e[1]+"</td>"+"<td>"+e[2]+"</td>"
							+"<td>"+e[3]+"</td>"+"<td>"+e[4]+"</td>"+
							"<td><a id='"+e[0]+"'class='btn btn-secondary text-white' data-toggle='modal' data-target='#editarIntegrante' value'"+e[0]+"'>Editar</a></td>"+
							"<td><a id='"+e[0]+"e' class='btn btn-danger text-white btnEli' data-toggle='modal' data-target='#eliminarIntegrante' data-value'"+e[0]+"'>Eliminar</a></td>");
					})
				},
				error: function(){
					console.log("Error.");
				}
			})
			.fail( function( jqXHR, textStatus, errorThrown ) {
			    console.log(textStatus + errorThrown);
			});
	}

	function actualizarListaIntegrantes(){
		/*UTILIDAD: actualiza la lista de integrantes registrados.*/
		$("#integrantes").click(function(){
			var tabla = {
				funcion: 'obtenerIntegrantes' 
			};
			$("#contenido-tabla").empty();
			mostrarListaIntegrantes(0);
		});
	}

	function limpiarFormulario(formulario){
		// UTILIDAD: limpia el formulario de registro de integrantes.
		$("#"+formulario).trigger("reset");
	}

	function registrarIntegrante(datos){
		/*UTILIDAD: registra a un integrante mediante AJAX.*/
		console.log("Registrar integrante");
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'text',
			encode: false,
			success: function(respuesta){
				// console.log(respuesta);
				alert("Datos agregados correctamente....");
				mostrarListaIntegrantes(1);
				$("#agregarIntegrante").modal("hide");
			},
			error: function(jqXHR, textStatus, errorThrown){
				console.log(textStatus + errorThrown);
			}
		});
	}

	function buscarClaveIntegrante(clave){
		/*UTILIDAD: verifica si la clave existe antes de registrar un integrante nuevo.*/
		var datos = {
			funcion: 'buscarClave',
			dato: clave,
		};
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'json',
			encode: false,
			success: function(respuesta){
				var alerta = document.querySelector("label[name=existeClave]");
				var btnAgregar = document.querySelector("#btnAgregarInt");
				if (typeof respuesta == "object") {
					alerta.style = "display: block";
					alerta.innerHTML = "La clave ya existe."+"<br>";
					alerta.style = "color: red";
					btnAgregar.disabled = "none";
				}else{
					alerta.style = "display: none";
					btnAgregar.disabled = "";
				}
			},
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(jqXHR + textStatus + errorThrown);
		});
	}

	function eliminarIntegrante(clave){
		/*UTILIDAD: elimina a un integrante de ka base de datos*/
		datos = {
			funcion:'eliminarIntegrante',
			cve: clave
		}
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'text',
			encode: false,
			success: function(respuesta){
				alert("Registro eliminado correctamente...");
				var tabla = {
					funcion: 'obtenerIntegrantes' 
				};
				$("#contenido-tabla").empty();
				mostrarListaIntegrantes(0);
				$("#eliminarIntegrante").modal("hide");
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert("No se ha podido eliminar el registro seleccionado... Por favor intente nuevamente.");
				// console.log(textStatus + errorThrown);
			}
		});
	}

/*********************Funciones de programa*******************/
	function mostrarListaProgramas(pagina){
		var tabla = {
			funcion: 'obtenerProgramas',
			num_reg: pagina,
		};
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: tabla,
			dataType: 'json',
			encode: true,
			success: function(respuesta){
				// $("#paginadorIntegrantes").load(':div[id=paginadorIntegrantes]');
				var i = 0;
				$("#tabla-programa").empty();
				if (pagina == 0 || pagina == 1) {
					i = 1;
				}else if(pagina >= 2){
					i = (pagina*10)-9;
				}
				respuesta.forEach(function(e){
					$("#tabla-programa").append("<tr>"+"<td>"+(i++)+"</td>"+"<td>"+e[0]+"</td>"+"<td>"+e[1]+"</td>"+
							"<td><a id='"+e[0]+"editar'class='btn btn-secondary text-white btnEditarPrograma' data-toggle='modal' data-target='#editarPrograma' value'"+e[0]+"'>Editar</a></td>"+
							"<td><a id='"+e[0]+"eliminar' class='btn btn-danger text-white btnEliminarPrograma' data-toggle='modal' data-target='#eliminarPrograma' data-value'"+e[0]+"'>Eliminar</a></td>");
				})
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function buscarClavePrograma(clave){
		/*UTILIDAD: verifica si la clave existe antes de registrar un integrante nuevo.*/
		var datos = {
			funcion: 'buscarClavePrograma',
			dato: clave,
		};
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'json',
			encode: false,
			success: function(respuesta){
				var alerta = document.querySelector("label[name=existeClavePrograma]");
				var btnAgregar = document.querySelector("#btnAgregarPrograma");
				if (typeof respuesta == "object") {
					alerta.style = "display: block";
					alerta.innerHTML = "La clave ya existe."+"<br>";
					alerta.style = "color: red";
					btnAgregar.disabled = "none";
				}else{
					alerta.style = "display: none";
					btnAgregar.disabled = "";
				}
			},
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(jqXHR + textStatus + errorThrown);
		});
	}

	function agregarPrograma(){
		/*UTILIDAD: obtiene los campos del formulario programa e inicia el registro del mismo.*/
		var campoClavePrograma = document.getElementsByName("campoClavePrograma")[0].value;
		var campoNombrePrograma = document.getElementsByName("campoNombrePrograma")[0].value;
		campoClavePrograma = campoClavePrograma.toUpperCase();
		var datos = {
			funcion: 'registrarPrograma',
			clave: campoClavePrograma,
			nombre: campoNombrePrograma,
		};
		registrarPrograma(datos);
	}

	function registrarPrograma(datos){
		/*UTILIDAD: registra un programa mediante AJAX.*/
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'text',
			encode: false,
			success: function(respuesta){
				// $("#tabla-programa").empty();
				mostrarListaProgramas(0);
				limpiarFormulario("formularioPrograma");
				$("#agregarPrograma").modal("hide");
			},
			error: function(jqXHR, textStatus, errorThrown){
				console.log(textStatus + errorThrown);
			}
		});
	}

	function llenarModalEditarPrograma(clave){
		/*UTILIDAD: escribe datos en el formulario programa.*/
		obtenerDatosEditarPrograma(clave);
	}

	function obtenerDatosEditarPrograma(clave){
		/*UTILIDAD: obtiene los datos de un programa.*/
		datos = {
			funcion:'obtenerPrograma',
			clave:clave
		};
		$.ajax({
				type: 'POST',
				url: "vista/modulos/ajax.php",
				data: datos,
				dataType: 'json',
				encode: false,
				success: function(e){
					$('input[name="campoEditarClavePrograma"]').attr("value", e[0]);
					$('input[name="campoEditarClavePrograma"]').attr("disabled", "");
					$('input[name="campoEditarNombrePrograma"]').attr("value", (e[1]));		
				},
				error: function(){
					console.log("Error.");
				}
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function obtenerDatosActualizarPrograma(){
		/*UTILIDAD: actualiza los datos del programa previamente registrado.*/
		var campoClave = document.getElementsByName("campoEditarClavePrograma")[0].value;
		var campoNombre = document.getElementsByName("campoEditarNombrePrograma")[0].value;
		campoClave = campoClave.toUpperCase();
		var datos = {
			funcion: 'actualizarPrograma',
			clave: campoClave,
			nombre: campoNombre,
		};
		actualizarPrograma(datos);
	}

	function actualizarPrograma(datos){
		/*UTILIDAD: actualiza los datos del programa previamente registrado.*/
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'text',
			encode: false,
			success: function(respuesta){
				alert("Datos actualizados correctamente...");
				$("#tabla-programa").empty();
				mostrarListaProgramas(0);
				$("#editarPrograma").modal("hide");
			},
			error: function(){
				alert("No se ha podido actualizar.. intente nuevamente.");
			}
		});
	}

	function llenarModalEliminarPrograma(clave){
		obtenerDatosEliminarPrograma(clave);
	}

	function obtenerDatosEliminarPrograma(clave){
		/*UTILIDAD: obtiene los datos de un integrante.*/
		datos = {
			funcion:'obtenerPrograma',
			clave:clave
		};
		$.ajax({
				type: 'POST',
				url: "vista/modulos/ajax.php",
				data: datos,
				dataType: 'json',
				encode: false,
				success: function(e){
					// console.log(e);
					$(':input[name="campoEliminarClavePrograma"]').attr("value", e[0]);
					$(':input[name="campoEliminarClavePrograma"]').attr("readonly", "readonly");
					$(':input[name="campoEliminarNombrePrograma"]').attr("value", (e[1]));
					$(':input[name="campoEliminarNombrePrograma"]').attr("readonly", "readonly");
				},
				error: function(){
					console.log("Error.");
				}
			})
			.fail( function( jqXHR, textStatus, errorThrown ) {
			    console.log(textStatus + errorThrown);
			});
	}

	function eliminarPrograma(clave){
		/*UTILIDAD: elimina a un programa de la base de datos*/
		datos = {
			funcion:'eliminarPrograma',
			cve: clave
		}
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'text',
			encode: false,
			success: function(respuesta){
				alert("Registro eliminado correctamente...");
				$("#tabla-programa").empty();
				mostrarListaProgramas(0);
				$("#eliminarPrograma").modal("hide");
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert("No se ha podido eliminar el registro seleccionado... Por favor intente nuevamente.");
				// console.log(textStatus + errorThrown);
			}
		});
	}

/*********************Funciones de actividad*******************/
	function mostrarListaActividades(pagina){
		var tabla = {
			funcion: 'obtenerActividades',
			num_reg: pagina,
		}
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: tabla,
			dataType: 'json',
			encode: true,
			success: function(respuesta){
				$("#paginadorIntegrantes").load(':div[id=paginadorIntegrantes]');
				var i = 0;
				$("#contenido-actividades").empty();
				if (pagina == 0 || pagina == 1) {
					i = 1;
				}else if(pagina >= 2){
					i = (pagina*10)-9;
				}
				respuesta.forEach(function(e){
					$("#contenido-actividades").append("<tr>"+"<td>"+(i++)+"</td>"+"<td>"+e[0]+e[1]+"</td>"+"<td>"+e[2]+"</td>"+
						
						"<td><a id='"+e[0]+e[1]+"editar'class='btn btn-secondary text-white btnEditarActividad' data-toggle='modal' data-target='#editarActividad' value'"+e[0]+e[1]+"'>Editar</a></td>"+
						"<td><a id='"+e[0]+e[1]+"eliminar' class='btn btn-danger text-white btnEliminarActividad' data-toggle='modal' data-target='#eliminarActividad' data-value'"+e[0]+e[1]+"'>Eliminar</a></td>");
				})
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function despliegueProgramas(seleccion, clave_actividad){
		/*UTILIDAD: muestra en un select los programas disponibles.*/
		var tabla = {
			funcion: 'obtenerDespliegueProgramas' 
		};
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: tabla,
			dataType: 'json',
			encode: true,
			success: function(respuesta){
				respuesta.forEach(function(e){
					$("#"+seleccion).append('<option value='+e[0]+'>'+e[0]+" - "+e[1]+'</option>');
					// if (obtenerOpcionPrograma() == "") {
						$(":input[name="+clave_actividad+"]").prop('disabled', true);
					// }
					
				})
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function obtenerOpcionPrograma(){
		$('select').on('change', function (e) {
			console.log("Hola David...");
		    var optionSeleccionada = $("option:selected", this);
		    var valorSeleccion = this.value;
		    console.log(optionSeleccionada + " - " + valorSeleccion);
		 //    if (valorSeleccion != "") {
			// 	$(":input[name=campoRegistroActividad]").prop('disabled', false);
			// }
		});
	}

	function buscarClaveActividad(programa, actividad){
		var datos = {
			funcion: 'buscarClaveActividad',
			programa: programa,
			actividad: actividad,
		};
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'json',
			encode: false,
			success: function(respuesta){
				// console.log(respuesta);
				var alerta = document.querySelector("label[name=existeClaveActividad]");
				var btnAgregar = document.querySelector("#btnAgregarActividad");
				if (typeof respuesta == "object") {
					alerta.style = "display: block";
					alerta.innerHTML = "La clave ya existe."+"<br>";
					alerta.style = "color: red";
					btnAgregar.disabled = "none";
				}else{
					alerta.style = "display: none";
					btnAgregar.disabled = "";
				}
			}
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(jqXHR + textStatus + errorThrown);
		});
	}

	function agregarActividad(){
		/*UTILIDAD: obtiene los campos del formulario programa e inicia el registro del mismo.*/
		var campoClavePrograma = $('select[name=campoSeleccionPrograma]').val();
		var campoClaveActividad = document.getElementsByName("campoRegistroActividad")[0].value;
		var campoNombrePrograma = document.getElementsByName("campoRegistroDescripcion")[0].value;
		var campoPrioridadPrograma = document.getElementsByName("campoRegistroPrioridad")[0].value;
		var datos = {
			funcion: 'registrarActividad',
			cve_prog: campoClavePrograma,
			cve_act: campoClaveActividad,
			nombre: campoNombrePrograma,
			prioridad: campoPrioridadPrograma,
		};
		registrarActividad(datos);
	}

	function registrarActividad(datos){
		/*UTILIDAD: registra un programa mediante AJAX.*/
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'text',
			encode: false,
			success: function(respuesta){
				alert("Datos agregados correctamente....");
				mostrarListaActividades(1);
				$("#agregarActividad").modal("hide");
			},
			error: function(jqXHR, textStatus, errorThrown){
				console.log(textStatus + errorThrown);
			}
		});
	}

	function llenarModalEditarActividad(programa, actividad){
		/*UTILIDAD: escribe datos en el formulario Actividad.*/
		obtenerDatosEditarActividad(programa, actividad);
	}

	function obtenerDatosEditarActividad(prog, acti){
		/*UTILIDAD: obtiene los datos de un Actividad.*/
		datos = {
			funcion:'obtenerActividad',
			programa:prog,
			actividad:acti,
		};
		$.ajax({
				type: 'POST',
				url: "vista/modulos/ajax.php",
				data: datos,
				dataType: 'json',
				encode: false,
				success: function(e){
					$('input[name=campoEditarSeleccionPrograma]').attr("value", e[0]);
					$('input[name=campoEditarSeleccionPrograma]').attr("disabled", "");
					$('input[name="campoEditarClaveActividad"]').attr("value", e[1]);
					$('input[name="campoEditarClaveActividad"]').attr("disabled", "");
					$('input[name="campoEditarDescripcion"]').attr("value", (e[2]));
					$('input[name="campoEditarPrioridad"]').attr("value", (e[3]));		
				},
				error: function(){
					console.log("Error.");
				}
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function despliegueProgramasEditarActividad(){
		/*UTILIDAD: muestra en un select los programas disponibles.*/
		var tabla = {
			funcion: 'obtenerDespliegueProgramas' 
		};
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: tabla,
			dataType: 'json',
			encode: true,
			success: function(respuesta){
				respuesta.forEach(function(e){
					$("#campoAgregaPrograma").append('<option value='+e[0]+'>'+e[0]+" - "+e[1]+'</option>');
					// if (obtenerOpcionPrograma() == "") {
						$(":input[name=campoRegistroActividad]").prop('disabled', true);
					// }
					
				})
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function obtenerDatosActualizarActividad(){
		/*UTILIDAD: actualiza los datos del programa previamente registrado.*/
		var campoPrograma = $('input[name=campoEditarSeleccionPrograma]').val();
		var campoActividad = $('input[name="campoEditarClaveActividad"]').val();
		var campoDescripcion = $('input[name="campoEditarDescripcion"]').val();
		var campoPrioridad = $('input[name="campoEditarPrioridad"]').val();
		var datos = {
			funcion: 'actualizarActividad',
			programa: campoPrograma,
			actividad: campoActividad,
			descripcion: campoDescripcion,
			prioridad: campoPrioridad
		};
		actualizarActividad(datos);
	}

	function actualizarActividad(datos){
		/*UTILIDAD: actualiza los datos del programa previamente registrado.*/
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'text',
			encode: false,
			success: function(respuesta){
				alert("Datos actualizados correctamente...");
				mostrarListaActividades(1);
				$("#editarActividad").modal("hide");
			},
			error: function(){
				alert("No se ha podido actualizar.. intente nuevamente.");
			}
		});
	}

	function llenarModalEliminarActividad(programa, actividad){
		obtenerDatosEliminarActividad(programa, actividad);
	}

	function obtenerDatosEliminarActividad(programa, actividad){
		/*UTILIDAD: obtiene los datos de un integrante.*/
		datos = {
			funcion:'obtenerActividad',
			programa:programa,
			actividad:actividad,
		};
		$.ajax({
				type: 'POST',
				url: "vista/modulos/ajax.php",
				data: datos,
				dataType: 'json',
				encode: false,
				success: function(e){
					$('input[name=campoEliminarSeleccionPrograma]').attr("value", e[0]);
					$('input[name=campoEliminarSeleccionPrograma]').attr("readonly", "readonly");
					$('input[name="campoEliminarClaveActividad"]').attr("value", e[1]);
					$('input[name="campoEliminarClaveActividad"]').attr("readonly", "readonly");
					$('input[name="campoEliminarDescripcion"]').attr("value", (e[2]));
					$('input[name="campoEliminarDescripcion"]').attr("readonly", "readonly");
					$('input[name="campoEliminarPrioridad"]').attr("value", (e[3]));	
					$('input[name="campoEliminarPrioridad"]').attr("readonly","readonly");	
				},
				error: function(){
					console.log("Error.");
				}
			})
			.fail( function( jqXHR, textStatus, errorThrown ) {
			    console.log(textStatus + errorThrown);
			});
	}

	function eliminarActividad(programa, actividad){
		/*UTILIDAD: elimina a un programa de la base de datos*/
		datos = {
			funcion:'eliminarActividad',
			prog: programa,
			actividad: actividad,
		}
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'text',
			encode: false,
			success: function(respuesta){
				alert("Registro eliminado correctamente...");
				mostrarListaActividades(0);
				$("#eliminarActividad").modal("hide");
			},
			error: function(jqXHR, textStatus, errorThrown){
				alert("No se ha podido eliminar el registro seleccionado... Por favor intente nuevamente.");
				// console.log(textStatus + errorThrown);
			}
		});
	}

/*********************Funciones asignación*****************************/
	function crearAsignacion(fecha, programa, actividad, integrante){
		/*UTILIDAD: */
		var datos = {
			funcion: 'agregarFechaAsignacion',
			fecha: fecha,
			integrante: integrante,
			programa: programa,
			actividad: actividad
		}
		console.log(datos);
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'text',
			encode: true,
			success: function(respuesta){
				console.log(respuesta);
				alert("Asignación realizada correctamente.");
				reiniciarFormularioAsignacion();
				// $("#integrante-actividad-rep").empty();
				// mostrarReporteActividadIntegrante();
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function obtenerReporteFecha(fecha){
		/*UTILIDAD: */
		var datos = {
			funcion: 'reporteFecha',
			fecha: fecha
		}
		console.log(datos);
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'json',
			encode: true,
			success: function(respuesta){
				console.log(respuesta);
				$("#reporte-detallado").empty();
				respuesta.forEach(function(e){
					$("#reporte-detallado").append('<tr><td>'+e[0]+'</td><td>'+e[1]+'</td><td>'+e[2]+' '+e[3]+' '+e[4]+
						'</td><td>'+e[5]+'</td></tr>');
				});
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function obtenerSeleccionIntegrantes(){
		datos = {
			funcion:'obtenerSeleccionIntegrantes',
			fecha: fecha
		}
		$.ajax({
				type: 'POST',
				url: "vista/modulos/ajax.php",
				data: datos,
				dataType: 'json',
				encode: false,
				success: function(respuesta){
					console.log(respuesta);
					respuesta.forEach(function(e){
						$("#selecIntegrante").append('<option value="'+e[0]+'">'+e[0]+' - '+e[1]+' '+e[2]+' '+e[3]+'</option>');
					})
					
				},error: function(){
					console.log("Error.");
				}
			})
			.fail( function( jqXHR, textStatus, errorThrown ) {
			    console.log(textStatus + errorThrown);
			});
	}

	function mostrarSeleccionListaPrograma(){
		/*UTILIDAD: muestra en un select los programas disponibles.*/
		var tabla = {
			funcion: 'obtenerDespliegueProgramas' 
		};
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: tabla,
			dataType: 'json',
			encode: true,
			success: function(respuesta){
				respuesta.forEach(function(e){
					$("#select-programas").append('<option value='+e[0]+'>'+e[0]+" - "+e[1]+'</option>');
					// if (obtenerOpcionPrograma() == "") {
						
					// }
					
				})
				$(":input[name=campoRegistroActividad]").prop('disabled', true);
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function mostrarSeleccionListaActividades(prog){
		/*UTILIDAD: muestra en un select los programas disponibles.*/
		var tabla = {
			funcion: 'obtenerDespliegueActividades',
			programa: prog
		};
		// console.log(tabla);
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: tabla,
			dataType: 'json',
			encode: true,
			success: function(respuesta){
				$('#selecIntegrante').attr('disabled', '');
				// $("#select-actividades").append("Elija una actividad");
				respuesta.forEach(function(e){
					$("#select-actividades").append('<option value='+e[0]+'>'+e[1]+'</option>');
				})

			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function obtenerSeleccionIntegrantes2(){
		datos = {
			funcion:'obtenerSeleccionIntegrantes2'
		}
		$.ajax({
				type: 'POST',
				url: "vista/modulos/ajax.php",
				data: datos,
				dataType: 'json',
				encode: false,
				success: function(respuesta){
					respuesta.forEach(function(e){
						$("#selecIntegranteAct").append('<option value="'+e[0]+'">'+e[0]+' - '+e[1]+' '+e[2]+' '+e[3]+'</option>');
					})
					
				},error: function(){
					console.log("Error.");
				}
			})
			.fail( function( jqXHR, textStatus, errorThrown ) {
			    console.log(textStatus + errorThrown);
			});
	}

	function mostrarSeleccionListaActividades2(prog){
		/*UTILIDAD: muestra en un select los programas disponibles.*/
		var tabla = {
			funcion: 'obtenerDespliegueActividades2',
			programa: prog
		};
		// console.log(tabla);
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: tabla,
			dataType: 'json',
			encode: true,
			success: function(respuesta){
				respuesta.forEach(function(e){
					$("#select-actividadesProg").append('<option value='+e[0]+"-"+e[1]+'>'+e[0]+e[1]+" - "+e[2].substr(0,50)+'</option>');
				})
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function asignarActividadIntegrante(integrante, programa, actividad){
		/*UTILIDAD: muestra en un select los programas disponibles.*/
		var datos = {
			funcion: 'asignarActividadIntegrante',
			integrante: integrante,
			programa: programa,
			actividad: actividad
		};
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'text',
			encode: true,
			success: function(respuesta){
				// alert(respuesta);
				$("#integrante-actividad-rep").empty();
				mostrarReporteActividadIntegrante();
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function existeAsignacion(integrante, programa, actividad){
		/*UTILIDAD: muestra en un select los programas disponibles.*/
		var datos = {
			funcion: 'existeAsignacion',
			integrante: integrante,
			programa: programa,
			actividad: actividad
		};
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'text',
			encode: true,
			success: function(respuesta){
				// console.log(respuesta);
				if (respuesta == 'false') {
					asignarActividadIntegrante(integrante, programa, actividad);
					// existenRegistrosActividadIntegrante();
					// mostrarReporteActividadIntegrante();
				}else{
					alert("Ya existe la asigación de " + programa + actividad + " a " + integrante);
				}
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function mostrarReporteActividadIntegrante(){
		var datos = {
			funcion: 'reporteAsignacionIntegrante'
		};
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'json',
			encode: true,
			success: function(respuesta){
				var hola = "";
				respuesta.forEach(function(e){
					if (hola == e[0]) {
						$("#integrante-actividad-rep").append("<tr><td></td><td>"+
						"<td>"+e[4]+"</td><td><a class='btn btn-danger btnEliminarIA text-white' id='"+e[0]+"-"+e[5]+"-"+e[6]+"'>Eliminar</a></td></tr>");
					}else{
						$("#integrante-actividad-rep").append("<tr><td>"+e[0]+"</td><td>"+e[1]+" "+e[2]+" "+e[3]+
						"<td>"+e[4]+"</td><td><a class='btn btn-danger btnEliminarIA text-white' id='"+e[0]+"-"+e[5]+"-"+e[6]+"'>Eliminar</a></td></tr>");
					}
					hola = e[0];
				});
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function eliminarAsignacionIntegrante(integrante, programa, actividad){
		var datos = {
			funcion: 'eliminarAsignacionIntegrante',
			integrante: integrante,
			programa: programa,
			actividad:actividad
		};
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'json',
			encode: true,
			success: function(respuesta){
				// console.log(respuesta);
				$("#integrante-actividad-rep").empty();
				mostrarReporteActividadIntegrante(integrante, programa, actividad);
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function mostrarIntegrantesPorActividad(){
		var datos = {
			funcion: 'mostrarIntegrantesPorActividad'
		};
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'json',
			encode: true,
			success: function(respuesta){
				console.log(respuesta);
				respuesta.forEach(function(e){
					$("#selecIntegrante").append('<option>'+e[0]+" - "+e[1]+" "+e[2]+" "+e[3]+'</option>');
				});
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function existenRegistrosActividadIntegrante(){
		/*UTILIDAD: muestra en un select los programas disponibles.*/
		var datos = {
			funcion: 'existenRegistrosActInt',
		};
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'text',
			encode: true,
			success: function(respuesta){
				console.log(respuesta);
				if (respuesta == "0") {
					$("#alertaRegistros").css({display: 'inline'});
				}else if(respuesta = "1"){
					$("#alertaRegistros").css({display: 'none'});
					$("#tabla-integrante-actividad").css({display: "block"});
					mostrarReporteActividadIntegrante();
				}else{
					$("#tabla-integrante-actividad").empty();
					mostrarReporteActividadIntegrante();
				}
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function mostrarIntegrantesDisponibles(fecha, prog, acti){
		/*UTILIDAD:*/
		var datos = {
			funcion: 'mostrarIntegrantesDisponibles',
			fecha: fecha,
			programa: prog,
			actividad: acti,
		};
		$.ajax({
			type: 'POST',
			url: "vista/modulos/ajax.php",
			data: datos,
			dataType: 'json',
			encode: true,
			success: function(respuesta){
				console.log(respuesta);
				// $('#selecIntegrante').removeAttr('disabled');
				$('#selecIntegrante').append('Elija un integrante');
				respuesta.forEach(function(e){
					$("#selecIntegrante").append("<option value='"+e[0]+"'>"+e[0]+" - "+e[1]+" "+e[2]+" "+e[3]+"</option>")
				});
				
			},
			error: function(){
				console.log("Error.");
			}
			
		})
		.fail( function( jqXHR, textStatus, errorThrown ) {
		    console.log(textStatus + errorThrown);
		});
	}

	function camposVacios(){
		/*UTILIDAD: */
		var fecha = $("#fechaAsignacion").val();
		var programa = $("#select-programas").val();
		var actividad = $("#select-actividades").val();
		var integrante = $("#selecIntegrante").val();
		if (fecha == "" || programa == "" || actividad == "" || integrante == "") {
			return true
		}else{
			crearAsignacion(fecha, programa, actividad, integrante);
		}
	}

	function reiniciarFormularioAsignacion(){
		/*UTILIDAD*/
		$("#select-programas").empty().append('<option>Elija un programa</option>');
		mostrarSeleccionListaPrograma();
		$("#select-actividades").empty().append('<option>Elija una actividad</option>');
		// mostrarSeleccionListaActividades()
		$('#selecIntegrante').attr('disabled', '');
	}




