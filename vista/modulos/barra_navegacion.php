<?php session_start();?>
    <nav class="navbar navbar-light bg-primary navbar-expand-md fixed-top">
        <div class="container">
            <a href="#" class="navbar-brand">
                <p class="h3 text-white">LOGOTIPO</p>
            <!-- AGREGAR LOGO DE LA PAGINA -->
            </a>
            <button type="button" class="navbar-toggler bg-white" data-toggle="collapse" data-target="#menu-principal" aria-controls="menu-principal" aria-expanded="false" aria-label="Desplegar menu de navegación">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="menu-principal">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a href="index.php?accion=inicio" class="nav-link text-white">Inicio</a>
                    </li>
                    <li class="nav-item">
                        <a href="index.php?accion=hoy" class="nav-link text-white">Hoy</a>
                    </li>
                    <li class="nav-item">
                        <a href="index.php?accion=semana" class="nav-link text-white">Semana</a>
                    </li>
                    <li class="nav-item">
                        <a href="index.php?accion=mes" class="nav-link text-white">Mes</a>
                    </li>
                    <li class="nav-item">
                        <a href="index.php?accion=contacto" class="nav-link text-white">Contacto</a>
                    </li>
                    <?php
                        session_start();
                        if(isset($_SESSION["usuario"])){
                            echo '<li class="nav-item">
                                    <a href="index.php?accion=cerrarSesion" class="nav-link text-white">Salir</a>
                                  </li>';
                        }else{
                            echo '<li class="nav-item">
                                    <a href="index.php?accion=inicioSesion" class="nav-link text-white">Ingreso</a>
                                  </li>';
                        }
                    ?>
                    
                </ul>
           </div>
        </div>
    </nav>
