<?php             
    $num_paginas = 10;
    $objeto = new Modelo();
    $num_actividad = $objeto->contarProgramasModelo("programa");
    if ($num_actividad[0] > 0) {
    $paginas = ceil($num_actividad[0]/$num_paginas);
    echo '<nav aria-label="Page navigation example">
                  <ul class="pagination">';
    for ($i=0; $i < $paginas; $i++) { 
      echo '<li class="page-item"><a class="page-link" href="#&pagina='.($i+1).'">'.($i+1).'</a></li>';
    } 
      echo '</ul>
        </nav>';
    }
?>