<div class="card">
	<div class="card-header" role="tab" id="headingOne">
		<h5 class="mb-0">
			<a class="collapsed" id="tarjeta" data-toggle="collapse" href="#perro" aria-expanded="true" aria-controls="collapseOne">
				Tarjetas
    		</a>
		</h5>
	</div>
	<div id="perro" class="collapse" role="tabpanel" aria-labelledby="headingOne">
		<div class="card-body">
			<div class="">
				<div class="text-center table-responsive" id="contenidoIntegrante">
					<p>Nombres:</p>
					<select name="" id="lista-pub">
						<option value="">Elija una opcion</option>
					</select>
					<a href="#" class="btn btn-success">PRueba5</a>
					
				</div>
				<a class="text-right btn btn-success" href="" data-toggle="modal" data-target="#agregarIntegrante">Agregar integrante</a>
				<!-- Modal registrar integrantes -->
				<div class="modal" id="agregarIntegrante" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
				    		<div class="modal-header">
								<h5 class="modal-title" id="agregarIntegranteLabel">Agregar Integrante</h5>
				    		</div>
						<div class="modal-body">
							<form class="text-center" id="formularioRegIntegrante">
								<label>Clave</label><br>
								<input type="text" name="campoClave" required=""><br>
								<label name="existeClave" style="display: none;"></label>
								<label>Nombre</label><br>
								<input type="text" name="campoNombre" required="" ><br>
								<label>A. Paterno</label><br>
								<input type="text" name="campoApePat" required="" ><br>
								<label>A. Materno</label><br>
								<input type="text" name="campoApeMat" required="" ><br>
								<label>Sexo</label><br>
								<select name="campoSexo">
									<option value="H">Hombre</option>
									<option value="M">Mujer</option>
								</select>
							</form>
						</div>
					<div class="modal-footer">

				    	<button type="button" class="btn btn-secondary" id="btnCerrarRegistro" data-dismiss="modal">Cerrar</button>
				        <button type="button" class="btn btn-primary" id="btnAgregarInt">Agregar</button>
				      </div>
				    </div>
				  </div>
				</div>
				<!-- Modal editar integrantes -->
				<div class="modal" id="editarIntegrante" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
				    		<div class="modal-header">
								<h5 class="modal-title" id="editarIntegranteLabel">Editar Integrante</h5>
				    		</div>
						<div class="modal-body">

							<form class="text-center" id="formularioActIntegrante">
								<label>Clave</label><br>
								<input type="text" name="campoActClave" required=""><br>
								<label name="existeClave" style="display: none;"></label>
								<label>Nombre</label><br>
								<input type="text" name="campoActNombre" required="" ><br>
								<label>A. Paterno</label><br>
								<input type="text" name="campoActApePat" required="" ><br>
								<label>A. Materno</label><br>
								<input type="text" name="campoActApeMat" required="" ><br>
								<label>Sexo</label><br>
								<select name="campoActSexo">
									<option value="H">Hombre</option>
									<option value="M">Mujer</option>
								</select>
							</form>
						</div>
					<div class="modal-footer">
				    	<button type="button" class="btn btn-secondary" id="btnCerrarActualizacion" data-dismiss="modal">Cerrar</button>
				        <button type="button" class="btn btn-primary" id="btnActualizarInt">Actualizar</button>
				      </div>
				    </div>
				  </div>
				</div>
				<!-- Modal eliminar integrantes -->
				<div class="modal" id="eliminarIntegrante" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
					<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
				    		<div class="modal-header">
								<h5 class="modal-title" id="eliminarIntegranteLabel">Eliminar Integrante</h5>
				    		</div>
						<div class="modal-body">
							<form class="text-center" id="formularioEliIntegrante">
								<label>Clave</label><br>
								<input type="text" name="campoEliClave" required=""><br>
								<label name="existeClave" style="display: none;"></label>
								<label>Nombre</label><br>
								<input type="text" name="campoEliNombre" required="" ><br>
								<label>A. Paterno</label><br>
								<input type="text" name="campoEliApePat" required="" ><br>
								<label>A. Materno</label><br>
								<input type="text" name="campoEliApeMat" required="" ><br>
								<label>Sexo</label><br>
								<select name="campoEliSexo">
									<option value="H">Hombre</option>
									<option value="M">Mujer</option>
								</select>
							</form>
						</div>
					<div class="modal-footer">
				    	<button type="button" class="btn btn-secondary" id="btnCerrarEliminacion" data-dismiss="modal">Cerrar</button>
				        <button type="button" class="btn btn-danger" id="btnEliminarInt">Eliminar</button>
				      </div>
				    </div>
				  </div>
				</div>
			</div>
	</div>
</div>