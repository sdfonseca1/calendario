<div class="card">
    <div class="card-header" role="tab" id="headingFour">
        <h5 class="mb-0">
            <a id="asignarActividades" class="collapsed" data-toggle="collapse" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                Asignar actividades
            </a>
        </h5>
    </div>
    <div id="collapseFive" class="collapse" role="tabpanel" aria-labelledby="headingFour">
        <div class="card-body m-3">
            <div class="row">
                <div class="col-2">
                    <p>Fecha: </p>
                    <input type="date" id="fechaAsignacion">
                </div>
                <div class="col-3">
                    <p>Programa</p>
                    <select name="" id="select-programas">
                        <option value="">Elija un programa</option>
                    </select>
                </div>
                <div class="col-7">
                    <p>Actividad</p>
                    <select name="" id="select-actividades" disabled="">
                        <option value="">Elija una actividad</option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="col-5" id="seccionAsignacion">
                    <p>Integrante</p>
                    <select id="selecIntegrante">
                        <option value="">Elija un integrante</option>
                    </select>
                </div>
                <div class="col-2">
                    <br><br>
                    <button class="btn btn-primary" id="btnCrearAsignacion">Asignar</button>
                </div>
            </div>
            <hr>
            <h3>Reportes por fecha</h3>
            <div class="row" id="seccion-reporte-asig">
                <div class="col-2"> 
                    <p>Fecha:</p>
                    <input type="date" id="fechaReporte">
                </div>
                <div class="col-2">
                    <p>Acción:</p>
                    <button  class="btn btn-primary" id="btnReporteFecha">Reportar</button>
                </div>
                <div class="offset-8"></div>
                
            </div>
            <div class="" id="reportes">
                    <table class="table">
                      <thead class="thead-dark">
                        <tr>
                            <th>Fecha</th>
                            <th>Clave</th>
                            <th>Nombre</th>
                            <th>Actividad</th>
                        </tr>
                      </thead>
                      <tbody id="reporte-detallado">
                        
                      </tbody>
                    </table>
                </div>
        </div>

        
    </div>
</div>
