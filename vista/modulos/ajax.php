<?php

	require_once "../../controlador/controlador.php";
	require_once "../../modelo/modelo.php";

	$funcion = $_POST["funcion"];


	switch ($funcion) {
		case 'prueba':
			$respuesta = Controlador::pruebaControlador();
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerSeleccionIntegrantes':
			$respuesta = Controlador::obtenerSeleccionIntegrantesControlador();
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'mostrarIntegrantesPorActividad':
			$respuesta = Controlador::mostrarIntegrantesPorActividadControlador();
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'eliminarAsignacionIntegrante':
			$respuesta = Controlador::eliminarAsignacionIntegranteControlador($_POST);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'reporteAsignacionIntegrante':
			$respuesta = Controlador::reporteAsignacionIntegranteControlador();
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;			
		case 'existeAsignacion':
			$respuesta = Controlador::existeAsignacionControlador($_POST);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerSeleccionIntegrantes2':
			$respuesta = Controlador::obtenerSeleccionIntegrantesControlador2();
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerIntegrantes':
			$respuesta = Controlador::obtenerIntegrantesControlador($_POST['num_reg']);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerIntegrantesRegistrados':
			$respuesta = Controlador::obtenerIntegrantesControlador($_POST['num_reg']);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerProgramas':
			$respuesta = Controlador::obtenerProgramasControlador($_POST['num_reg']);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerActividades':
			$respuesta = Controlador::obtenerActividadesControlador($_POST['num_reg']);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'buscarClave':
			$respuesta = Controlador::buscarClaveControlador($_POST['dato']);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'registrarIntegrante':
			$respuesta = Controlador::registrarIntegranteControlador($_POST);
			// $respuesta = Controlador::latin1ToUTF8($respuesta);
			// header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'obtenerIntegrante':
			$respuesta = Controlador::obtenerIntegranteControlador($_POST['clave']);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			echo json_encode($respuesta);
			break;
		case 'actualizarIntegrante':
			$respuesta = Controlador::actualizarIntegranteControlador($_POST);
			echo $respuesta;
			break;
		case 'eliminarIntegrante':
			$respuesta = Controlador::eliminarIntegranteControlador($_POST['cve']);
			echo $respuesta;
			break;
		case 'buscarClavePrograma':
			$respuesta = Controlador::buscarClaveProgramaControlador($_POST['dato']);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'registrarPrograma':
			$respuesta = Controlador::registrarProgramaControlador($_POST);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			break;
		case 'obtenerPrograma':
			$respuesta = Controlador::obtenerProgramaControlador($_POST['clave']);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			echo json_encode($respuesta);
			break;
		case 'actualizarPrograma':
			$respuesta = Controlador::actualizarProgramaControlador($_POST);
			// var_dump($respuesta);
			echo $respuesta;
			break;
		case 'eliminarPrograma':
			$respuesta = Controlador::eliminarProgramaControlador($_POST['cve']);
			echo $respuesta;
			break;
		case 'obtenerDespliegueProgramas':
			$respuesta = Controlador::obtenerDespliegueProgramasControlador();
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			echo json_encode($respuesta);
			break;
		case 'obtenerDespliegueActividades':
			$respuesta = Controlador::obtenerDespliegueActividadesControlador($_POST['programa']);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			echo json_encode($respuesta);
			break;
		case 'obtenerDespliegueActividades2':
			$respuesta = Controlador::obtenerDespliegueActividadesControlador2();
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			echo json_encode($respuesta);
			break;
		case 'mostrarIntegrantesDisponibles':
			// var_dump($_POST);
			$respuesta = Controlador::mostrarIntegrantesDisponiblesControlador($_POST);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			// var_dump($respuesta);			
			echo json_encode($respuesta);
			break;
		case 'reporteFecha':
			$respuesta = Controlador::reporteFechaControlador($_POST['fecha']);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');		
			echo json_encode($respuesta);
			break;
		case 'buscarClaveActividad':
			$respuesta = Controlador::buscarClaveActividadControlador($_POST['programa'], $_POST['actividad']);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			header('Content-Type: application/json');
			echo json_encode($respuesta);
			break;
		case 'registrarActividad':
			$respuesta = Controlador::registrarActividadControlador($_POST);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			break;
		case 'obtenerActividad':
			$respuesta = Controlador::obtenerActividadControlador($_POST);
			$respuesta = Controlador::latin1ToUTF8($respuesta);
			echo json_encode($respuesta);
			break;
		case 'actualizarActividad':
			$respuesta = Controlador::actualizarActividadControlador($_POST);
			// var_dump($respuesta);
			echo $respuesta;
			break;
		case 'eliminarActividad':
			$respuesta = Controlador::eliminarActividadControlador($_POST);
			echo $respuesta;
			break;
		case 'asignarActividadIntegrante':
			$respuesta = Controlador::asignarActividadIntegranteControlador($_POST);
			echo $respuesta;
			break;
		case 'agregarFechaAsignacion':
			$respuesta = Controlador::crearFechaAsignacionControlador($_POST);
			echo $respuesta;
			break;
		case 'existenRegistrosActInt':
			$respuesta = Controlador::existenRegistrosActIntControlador();
			echo $respuesta;
			break;
		// case 'registroIntegrante':
			// $respuesta = Controlador::registroIntegranteControlador();
			// break;
		

	}




?>
