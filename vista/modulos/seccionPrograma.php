<div class="card">
    <div class="card-header" role="tab" id="headingTwo">
        <h5 class="mb-0">
            <a class="collapsed" id="programas" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                Programas
            </a>
        </h5>
    </div>
    <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
        <div class="card-body">
            <div>
                <div class="text-center table-responsive">
                    <form>
                        <table class="table table-sm" id="tabla-programa">
                            <thead class="bg-secondary text-white">
                                <tr>
                                    <th >Clave</th>
                                    <th >Nombre</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            <tbody id="contenido-programas">

                            </tbody>
                            </thead>
                            
                        </table>
                    </form>
                    <?php
                        require "vista/modulos/paginadorProgramas.php";
                    ?>
                </div>
            </div>
            <a class="text-right btn btn-success" href="" data-toggle="modal" data-target="#agregarPrograma">Agregar Programa</a>
            <!-- Modal registrar programas-->
            <div class="modal" id="agregarPrograma" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="agregarProgramaLabel">Agregar Programa</h5>
                        </div>
                    <div class="modal-body">
                        <form class="text-center" id="formularioPrograma">
                            <label>Clave del programa</label><br>
                            <input type="text" name="campoClavePrograma" required=""><br>
                            <label name="existeClavePrograma" style="display: none;"></label>
                            <label>Nombre del programa</label><br>
                            <input type="text" name="campoNombrePrograma" required=""><br>
                        </form>
                    </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-secondary" id="btnCerrarRegPrograma" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btnAgregarPrograma">Agregar</button>
                  </div>
                </div>
              </div>
            </div>

            <!-- Modal editar programa -->
            <div class="modal" id="editarPrograma" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editarProgramaLabel">Editar Programa</h5>
                        </div>
                    <div class="modal-body">
                        <form class="text-center">
                            <label>Clave del programa</label><br>
                            <input type="text" name="campoEditarClavePrograma" required=""><br>
                            <label>Nombre del programa</label><br>
                            <input type="text" name="campoEditarNombrePrograma" required=""><br>
                        </form>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="btnCerrarActualizacionPrograma" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btnActualizarPrograma">Actualizar</button>
                  </div>
                </div>
              </div>
            </div>



            <!-- Modal eliminar integrantes -->
            <div class="modal" id="eliminarPrograma" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="eliminarProgramaLabel">Eliminar Programa</h5>
                        </div>
                    <div class="modal-body">
                        <form class="text-center" id="formularioEliPrograma">
                            <label>Clave</label><br>
                            <input type="text" name="campoEliminarClavePrograma" required=""><br>
                            <label name="existeClave" style="display: none;"></label>
                            <label>Nombre</label><br>
                            <input type="text" name="campoEliminarNombrePrograma" required="" ><br>
                        </form>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="btnCerrarEliminacionPrograma" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-danger" id="btnEliminarPrograma">Eliminar</button>
                  </div>
                </div>
              </div>
            </div>






        </div>

    </div>
    
</div>