<?php
	session_start();
	if (!isset($_SESSION['usuario'])) {
		header("location:index.php?accion=inicioSesion");
	}
?>
<h1 class="my-5 text-center">Bienvenido administrador</h1>

<div id="accordion" role="tablist">
		<!-- Integrante -->
		<?php
			require_once("vista/modulos/seccionIntegrantes.php");
		?> 	
		<!-- Programa -->
		<?php
			require_once("vista/modulos/seccionPrograma.php");
		?>
		<!-- Actividad -->
		<?php
			require_once("vista/modulos/seccionActividad.php");
		?>
		<!-- Actividad -->
		<?php
			require_once("vista/modulos/seccionIntegranteActividad.php");
		?>
		<!-- Asignación de actividades -->
		<?php
			require_once("vista/modulos/seccionAsignación.php");
		?>
		<!-- prUEBAs -->
		<?php
			require_once("vista/modulos/tarjetas.php");
		?>
</div>