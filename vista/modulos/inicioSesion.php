	<div class="container text-center">
		<div class="row align-content-center m-0">
			<form class="offset-md-4" id="formSesion" method="post">
				<div class="form-group p-4 align-items-center">
					<h2 class="m-3">Ingresa a Calendario</h2>
					<label for="formGroupExampleInput">Usuario</label>
					<input name="clave" type="text" class="form-control" id="formGroupUsuario" placeholder="Usuario" required="true">
					<label for="formGroupExampleInput2">Contraseña</label>
					<input name="contrasena" type="password" class="form-control" id="formGroupContraseña" placeholder="Contraseña" required="true">
					
					<br><br>
					<button type="submit" class="btn btn-primary p-2" id="btnIniciar" value="">Iniciar sesion</button>
				</div>
			</form>
		</div>
	</div>

<?php
	$usuario = new Controlador();
	$usuario->ingresoUsuarioControlador();
	if(isset($_GET["accion"])){
		if($_GET["accion"] == "inicioSesionError"){
			echo "<script>alert ('Usuario y/o contraseña incorrectos.');</script>";
		}
	}
?>
		