<div class="card">
    <div class="card-header" role="tab" id="headingThree">
        <h5 class="mb-0">
            <a class="collapsed" id="actividades" data-toggle="collapse" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                Actividades
            </a>
        </h5>
    </div>
    <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
        <div class="card-body">
            <div>
                <div class="text-center table-responsive">
                    <form>
                        <table class="table table-sm" id="tabla-actividad">
                            <thead class="bg-secondary text-white">
                                <tr>
                                    <th>No.</th>
                                    <th>Clave</th>
                                    <th>Asignación</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            <tbody id="contenido-actividades">
                            </tbody>
                            </thead>
                            
                        </table>
                    </form>
                    <?php
                        require"vista/modulos/paginadorActividades.php";
                    ?>
                </div>
            </div>
            <a class="text-right btn btn-success agregarActividad" href="" data-toggle="modal" data-target="#agregarActividad">Agregar Actividad</a>
            <!-- Modal registro actividad-->
            <div class="modal" id="agregarActividad" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="agregarActividadLabel">Agregar Actividad</h5>
                        </div>
                    <div class="modal-body" id="formularioIntegrante">
                        <form class="text-center" id="formularioActividad">
                            <label>Clave programa</label><br>
                            <select name="campoSeleccionPrograma" id="campoAgregaPrograma">
                                <option value="">Elija uno</option>
                            </select><br>
                            <!-- <input type="text" name="campoPrograma" required=""><br> -->
                            <label>Clave actividad</label><br>
                            <input type="text" name="campoRegistroActividad" required=""><br>
                            <label name="existeClaveActividad" style="display: none;"></label>
                            <label>Descripción</label><br>
                            <input type="text" name="campoRegistroDescripcion" required=""><br>
                            <label>Prioridad</label><br>
                            <input type="number" name="campoRegistroPrioridad" required=""><br>
                        </form>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btnAgregarActividad">Agregar</button>
                  </div>
                </div>
              </div>
            </div>
             <!-- Modal editar actividad -->
            <div class="modal" id="editarActividad" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="editarActividadLabel">Editar Actividad</h5>
                        </div>
                    <div class="modal-body">

                        <form class="text-center" id="formularioActActividad">
                            <label>Clave programa</label><br>
                            <input type="text" name="campoEditarSeleccionPrograma" required=""><br>
                            <label>Clave actividad</label><br>
                            <input type="text" name="campoEditarClaveActividad" required=""><br>
                            <label name="existeClaveActividad" style="display: none;"></label>
                            <label>Descripción</label><br>
                            <input type="text" name="campoEditarDescripcion" required=""><br>
                            <label>Prioridad</label><br>
                            <input type="number" name="campoEditarPrioridad" required=""><br>
                        </form>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="btnCerrarActualizacion" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" id="btnActualizarActividad">Actualizar</button>
                  </div>
                </div>
              </div>
            </div>

            <!-- Modal eliminar atividad-->
            <div class="modal" id="eliminarActividad" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenteredLabel" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="eliminarActividadLabel">Eliminar Actividad</h5>
                        </div>
                    <div class="modal-body">
                        <form class="text-center" id="formularioEliActividad">
                            <label>Clave programa</label><br>
                            <input type="text" name="campoEliminarSeleccionPrograma" required=""><br>
                            <label>Clave actividad</label><br>
                            <input type="text" name="campoEliminarClaveActividad" required=""><br>
                            <label name="existeClaveActividad" style="display: none;"></label>
                            <label>Descripción</label><br>
                            <input type="text" name="campoEliminarDescripcion" required=""><br>
                            <label>Prioridad</label><br>
                            <input type="number" name="campoEliminarPrioridad" required=""><br>
                        </form>
                    </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="btnCerrarEliminacion" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-danger" id="btnEliminarActividad">Eliminar</button>
                  </div>
                </div>
              </div>
            </div>
















        </div>
    </div>
</div>