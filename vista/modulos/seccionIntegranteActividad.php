<div class="card">
    <div class="card-header" role="tab" id="headingSix">
        <h5 class="mb-0">
            <a id="asignarActInt" class="collapsed" data-toggle="collapse" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                Asignar actividades a integrantes
            </a>
        </h5>
    </div>
    <div id="collapseSix" class="collapse" role="tabpanel" aria-labelledby="headingSix">
        <div class="card-body row m-1">
            <div class="col-5" id="seccionAsignacion">
                <p>Integrante</p>
                <select id="selecIntegranteAct">
                    <option value="">Elija un integrante</option>
                </select>
            </div>
            <div class="col-6">
                <p>Actividad</p>
                <select name="" id="select-actividadesProg">
                    <option value="">Elija una actividad</option>
                </select>
            </div>
            <div class="col-1">
                <p>Accion</p>
                <button class="btn btn-success" id="btnAsignar">Asignar</button>
            </div>
        </div>
        <div class="alert alert-warning" role="alert" id="alertaRegistros">
            <strong>SIN REGISTROS EN LA BASE DE DATOS</strong>
        </div>
        <div id="tabla-integrante-actividad" style="display: none;">
            <table class="table" id="integrante-actividad">
                <thead class="thead-dark">
                    <tr>
                        <th>Clave</th>
                        <th>Nombre</th>
                        <th>Actividades</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="integrante-actividad-rep"></tbody>
            </table>
        </div>
        
    </div>
</div>
